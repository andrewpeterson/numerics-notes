\begingroup
\chapter{Bayesian inference}
\reading{B8}

% Probability and conditional probability.
\newcommand{\p}[1]{\ensuremath{P(\mathrm{#1})}}
\newcommand{\cp}[2]{\ensuremath{P(\mathrm{#1}|\mathrm{#2})}}
\newcommand{\pu}[1]{\ensuremath{P(\text{#1})}}
\newcommand{\pc}[2]{\ensuremath{P(\text{#1}|\text{#2})}}
\newcommand{\pum}[1]{\ensuremath{P{\left({#1}\right)}}}
\newcommand{\pcm}[2]{\ensuremath{P{\left({#1}|{#2}\right)}}}
\newcommand{\ym}{\ensuremath{y^\mathrm{obs}}}
\newcommand{\vym}{\ensuremath{\v{y}^\mathrm{obs}}}

\newcommand{\kb}{\ensuremath{k_\mathrm{B}}}
\newcommand{\etal}{\textit{et al.}}

Bayesian methods are a relatively new way of analyzing data with uncertainty; the use of Bayesian inference has really only become practical for the average user in recent years due to increased computational power and available computational algorithms.
(Although the specific software is still very much in flux.)

Before we get into the specifics of Bayesian statistics, we'll start with our end goal; then we'll build up to how this can be accomplished.
Imagine that we've measured some data and we want to perform linear regression.\footnote{We should also note that linear regression \emph{assumes} a linear model works to describe the data; this is an assumption in the classical regression method or the Bayesian method. Bayesian inference is also often used for model selection, but we won't cover that in our brief treatment here.}
In standard regression approaches (that we probably learned in high school---or that you get if you open a spreadsheet and ask it to fit a line), you'll get a ``best-fit'' line that reduces the residuals between the model prediction and the data points.
This is also what we did for nonlinear regression in \secref{sxn:regression}.
In the Bayesian inference methods we'll discuss today, we don't get a single fit line, but instead we'll get an \emph{ensemble} of lines as shown in Figure~\ref{fig:bayes-linear}.
This is really useful, as we can immediately know something about our confidence in the slope, in the intercept, and in the predictions of $y$ at various values of $x$ values.
Also plotted in Figure~\ref{fig:bayes-linear} are the distributions in the model parameters that we would see, as well as the distribution in the $y$ predictions for a given $x$.
You can imagine, by looking at the ensemble of slopes, that if we get outside the bounds of the original $x$ data that our models will quickly give a wide distribution of possible answers; we will immediately be penalized when we are trying to extrapolate.

Obviously, for a large sized ensemble we are going to need a computer to handle this.

\begin{figure}
\centering
\includegraphics[width=0.99 \textwidth]{f/bayes-linear/drawing.pdf}
\caption{\label{fig:bayes-linear} Top row: traditional best-fit line. Bottom: Bayesian ensemble. We instead get a distribution of values, in this case an ensemble. Each parameter has some distribution that we can look at. Importantly, when we make a prediction we automatically get some credible intervals around that prediction as well.
}
\end{figure}

This also works for other models.
For example, on your homework you plotted a block that was warming up, and it had unknown parameters $k$ and $h$.
You could do the same thing and get various curves that approximate that data (perhaps draw some) and know something about how sure you can be in your model's estimates.
In addition to parameter estimation, Bayesian methods are also commonly used for model selection and hypothesis testing.

We'll next go over the basic statistical concepts behind these methods.

\section{Brief statistics review: conditional probability \label{sxn:stat-review}}

\paragraph{Probability.}
We can consider the probability of a single event A occurring as $P(A)$.
\textit{E.g.}, the probability that it will snow tomorrow.
Maybe we can guess that it snows 20 days a year, so we might set this to be 20/365.

Probability is inherently subjective and in practice depends on the person assigning it; for example, someone else might find the real historical data of how many days a year it snowed in Providence.
Others might take into account things like what month it is---certainly it's more likely to snow in January than in July!
Most examples that we deal with in introductory courses are well-defined; such as flipping a coin where we can be pretty sure that $P(\text{heads})=0.5$.
However, in practice, most real probabilities are much more subjective.
This will become important in the Bayesian approach, particularly when we talk about ``prior'' beliefs in \secref{sxn:bayes-tricky}.

\paragraph{Conditional probability.}
If we know that event $\mathrm{B}$ occurred we can update our probability that A will occur, if they are related.
We can say that the probability of A also occurring, given that B occurred, is $P(\mathrm{A}|\mathrm{B})$.
So if B is ``It's July in Providence'', then we know that the probability $P(\text{It will snow today}|\text{It's July in Providence})$ is much lower than what we would have said for $P(\text{It will snow today})$ if we don't have the information that it's July in Providence.

\paragraph{Bayes' theorem.}
Bayes' theorem is fairly simple; it is often best explained (and remembered) via a Venn diagram; Figure~\ref{bayes-venn}.
For a simple analogy, imagine throwing a dart at the rectangle in an unbiased manner; that is, where the odds of striking any point within the rectangle $R$ are identical.
Then \p{A} (the probability that the dart lands in A) is equal to the area inside the ``A'' circle divided by the area of the rectangle ``R''; \p{B} is defined the same.
The ``joint'' probability that \emph{both} A and B occur (are hit by the same dart) is denoted $P(\mathrm{A} \cap \mathrm{B})$ and is represented by the small wedge of area where the A and B circles intersect, divided by the rectangle.

\begin{figure}
\centering
\includegraphics[width=0.49 \textwidth]{f/bayes-venn/drawing.pdf}
\caption{\label{bayes-venn} A Venn diagram.
}
\end{figure}

By intuition, we can write

\[ P (\mathrm{A} \cap \mathrm{B} ) = \p{B} \cp{A}{B}  = \p{A} \cp{B}{A} \]

\noindent
(Note the symbol ``$\cap$'' means joint probability.)
Bayes re-arranged this to give a relationship between the two conditional probabilities.

\[ \boxed{ \cp{B}{A}  = \frac{\cp{A}{B} \p{B}}{\p{A}} } \]

\noindent
So far, this is just an equation.
We'll look at its usefulness by examining a comic strip.

\paragraph{XKCD comic.}
Here, we should break and read the famous XKCD comic of Figure~\ref{fig:xkcd-bayes}.

According to XKCD, under the ``frequentist'' definition (classical statistics) it says there is a 35/36 chance that this did not occur by chance.
This seems kind of hard to argue with this logic.
Except it means we're about to die.
How would we do this with Bayes rule?
Let's try writing Bayes' theorem:

\begin{figure}
\centering
\includegraphics[width=0.8 \textwidth]{f/xkcd-bayes/frequentists_vs_bayesians.png}
\caption{XKCD's explanation of Bayesian statistics. From \url{https://xkcd.com/1132/}. (Available under a Creative Commons Attribution-NonCommercial License.)
\label{fig:xkcd-bayes}
}
\end{figure}


\[
	\pc{sun exploded}{machine ``yes''} = \frac{\pc{machine ``yes''}{sun exploded} \pu{sun exploded}}{\pu{machine ``yes''}} = \frac{\frac{35}{36} \cdot \frac{1}{10^{12}}}{{\sim}\frac{1}{36} } \approx \frac{35}{10^{12}}
\]

\noindent where I've plugged in some reasonable guesses for these numbers---so the sun is about 35 more times likely to have just exploded than if the machine had said no, but the odds are still (astronomically) low.

Let's discuss each of these terms, as this will be the guideline for how we use Bayes' theorem in future examples:
\begin{itemize}
\item
The first is obvious; if the detector gets a positive signal (the sun exploded), it will tell us ``yes'' with 35/36 odds. (That is, in this case it will tell us ``no'' only if both dice come up six.)
\item
We had to make up a number for \pu{sun exploded}.
This was a guess based on the age of the solar system.
This is sometimes the dissatisfying thing about the Bayesian approach: we have to explicitly put in our beliefs into this term.
But I'm pretty sure that didn't change our conclusion.

\item
The denominator is actually a little trickier than I showed. It is larger than 1/36 as it should have built into it the 1/36 chance that the dies come up 6's and one in a trillion chance that the sun exploded.
Properly, this could be written as a sum of the possibilities where the machine says ``yes''.

\begin{align*}
\pu{machine ``yes''} &= \pc{machine ``yes''}{sun fine} \pu{sun fine} + \pc{machine ``yes''}{sun exploded} \pu{sun exploded}\\
&= \frac{1}{36} \frac{10^9 - 1}{10^9} + \frac{35}{36} \frac{1}{10^9} \approx \frac{1}{36} 
\end{align*}


\end{itemize}

This example is illustrative of the use of Bayes theorem:
The common way we think of this is that we are ``updating'' our beliefs given new data.
We start with our \emph{prior belief}: what we assume are the odds that the sun exploded, which we express as \pu{sun exploded}.
When new data comes in (the machine said yes), we use Bayes theorem to update this belief and calculate \pc{sun exploded}{machine ``yes''}.
We call this our \emph{posterior belief}.

Additionally, we saw that the denominator involves a sum over possibilities.
This was relatively straightforward to write out in this case, but this will in general be a difficult term to ascertain.
The Metropolis / Monte Carlo approaches we encounter later will allow us to ignore this term.


\section{Parameter estimation (regression) with Bayes' theorem}

As mentioned in the introduction of this section, a common use of Bayes' theorem is in the estimation of parameters of a model, given data.
Other common tasks include hypothesis testing and model selection; however, if you understand the basics of parameter estimation it is simple to pick up the other techniques.

In our example in the introduction, we discussed using this approach for linear regression; you should still keep linear regression in mind as we discuss this example, but we'll keep the mathematics general so that your model can be any function $y = f(\v{x})$.
Here, we assume that the data comes in pairs of $(\v{x}, \ym)$; that is, the model takes in a vector \v{x} and outputs a scalar $y$.
We'll use $\ym$ to denote a measured value of $y$; that is, a data point.
Any of our particular data points can then be expressed as:

\begin{equation}\label{eq:bayes-model} \ym_i = f(\v{x}_i; \v{\theta}) + \varepsilon_i \end{equation}

\noindent
where the subscript $i$ means the $i^\mathrm{th}$ data point, $f$ is the assumed model (for example a straight line in linear regression), $\v{\theta}$ is a vector containing the model parameters (\textit{e.g.}, the slope and intercept of a line), and $\varepsilon_i$ is defined by the equation above: how much the $i^\mathrm{th}$ data point deviates from the model prediction.

$\varepsilon$ is typically referred to as the error.
In this example, we will assume the error is normally distributed.\footnote{This is frequently a good assumption, but you should make sure that it makes sense for your particular model.}
We can write this assumption as a conditional probability:

\[ \pcm{\varepsilon_i}{\sigma} = \frac{1}{\sqrt{2 \pi} \, \sigma} \exp \left\{ \frac{ - \left(\varepsilon_i \right)^2}{2 \sigma^2} \right\} \]

\noindent
In words, we would say ``Given a standard deviation $\sigma$, the probability of observing an error value of $\epsilon_i$ is ...''.
We don't know $\sigma$, but a neat thing about this approach is we will \emph{infer} it along with our model parameters; that is, we'll automatically understand the error of the model.

In the next line, we'll just plug in that $\varepsilon_i = \ym_i  - f(\v{x}_i;\v{\theta})$.
Therefore we can re-write this as:

\[ \pcm{\ym_i}{\v{\theta}, \sigma} = \frac{1}{\sqrt{2 \pi} \sigma} \exp \left\{ \frac{ - \left(\ym_i  - f(\v{x}_i; \v{\theta}) \right)^2}{2 \sigma^2} \right\} \]

\noindent
Note that our conditional probability now also has explicitly noted the value $\v{\theta}$.
We would say ``Given model parameters \v{\theta} and error standard deviation $\sigma$, the probability of observing measured point $\ym$ is ...''.

If all our measurements are contained in the vector $\vym$, we can write this as

\begin{equation}\label{eq:bayes-yobs} \pcm{\vym}{\v{\theta}, \sigma} =  \prod_{i=1}^N \frac{1}{\sqrt{2 \pi} \, \sigma}  \exp \left\{ \frac{ - \left( \ym_i - f(\v{x}_i; \v{\theta}) \right)^2}{2 \sigma^2} \right\} \end{equation}

\noindent
where $\prod$ is the product symbol and $N$ is the number of elements in \vym; we have just used the fact that the independent probabilities multiply.\footnote{\textit{E.g.}, if you flip two coins, the probability of them both coming up heads is $\frac{1}{4}$, or $\frac{1}{2} \times \frac{1}{2}$.}

But remember that the goal of this whole operation was to find $\v{\theta}$.
In the language of conditional probability, given some training points $\vym$, if I propose some model parameters $\v{\theta}$ (and error spread $\sigma$), what are the odds that this is the true choice?
This means that we have posed our problem backwards, but we can reverse it with Bayes' theorem.
This gives us what we want:

\begin{equation}\label{eq:bayes-inference} \boxed{ \pcm{\v{\theta}, \sigma}{\vym} = \frac{\pcm{\vym}{\v{\theta}, \sigma} \pum{\v{\theta}, \sigma}}{\pum{\vym}} } \end{equation}

\noindent
The above is the key working equation of Bayesian inference.

\subsection{The tricky terms\label{sxn:bayes-tricky}}

In the boxed equation, we have a simple expression for $\pcm{\vym}{\v{\theta},\sigma}$, but now we have two new terms we have to deal with in order to understand the odds.
In principle, these are the same terms we dealt with when discussing the XKCD comic, but let's discuss these again in this equation:

\begin{itemize}
\item
\textbf{Prior.}
\pum{\v{\theta}, \sigma} is known as the ``prior'' probability of our parameters.
Literally, this means that in the absence of any data, what do I expect these parameters to be?
For this reason, in the context of Bayesian statistics it is somewhat common to refer to probabilities as ``beliefs'' (and this is called the ``prior belief'' or just the ``prior'').
This is the controversial part of Bayesian statistics, but it reflects the subjective nature of assigning probabilities, and acknowledges this fact.
The subjective nature of assigning probabilities was discussed when we first reviewed probability in \secref{sxn:stat-review}.

\item
\textbf{(Posterior.)}
In the same manner as before, the left-hand side of the boxed equation is often called the posterior belief, and we can think of the equation as updating our prior belief into our posterior belief, given some new information (the data $\vym$).

\item
\textbf{Denominator.}
This term refers to the probability of measuring $\vym$ no matter the value of $\v{\theta}$, which by analogy with our sun exploding-sun example is

\begin{equation}\label{eq:bayes-tricky} \pum{\vym} = \int \pcm{\vym}{\v{\theta}, \sigma} \pum{\v{\theta}, \sigma} d\v{\theta} \, d\sigma \end{equation}

\noindent
This could be tricky to evaluate...we have to re-run our model at every conceivable value of \v{\theta} and $\sigma$ to evaluate the integral; the bounds on this integral are typically infinite (if there are no constraints on the allowable values of the parameters).
%(Since we typically don't have bounds on the possible values of $\v{theta}$ and $\sigma$, this integral's bounds would be infinite.)
However, note that this expression is independent of the \emph{particular} choice of $(\v{\theta}, \sigma)$ for which we are evaluating equation~\eqref{eq:bayes-inference}.
Fortunately, we can get around this at all by employing Monte Carlo integration, which we'll discuss in the following.

\end{itemize}


\section{Monte Carlo integration}

The clever technique that lets us avoid the tricky integral above is commonly called ``MCMC'' (Markov Chain Monte Carlo), but has its roots in the famous Metropolis algorithm first proposed at Los Alamos National Lab in the 1950s~\cite{Metropolis1953}.
We'll go over the concept of Monte Carlo integration in general, followed by the Metropolis algorithm in particular, finally showing how it is applied to Bayesian inference in \secref{sxn:mcmc}.
Really, the final section is all you need to know to use Bayesian inference, but the prior sections will give you more intuition into this method.

\subsection{Simple Monte Carlo integration (unbiased)}

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/monte-carlo-area-shapes/drawing.pdf}
\caption{We would like to know the area of the interior shape. An easy method would be to throw darts at the board and record the fraction inside the inner shape; this will approach its normalized area as the number of darts thrown gets large. \label{fig:monte-carlo-area-shapes}}
\end{figure}

Generally, a Monte Carlo (MC) method is one that relies on random number generation, and they can take many forms.
We start with a very basic idea of what MC is.
Consider Figure~\ref{fig:monte-carlo-area-shapes}, and suppose we want to know the area of the odd-looking shaded region.
Mathematically, we could express this as an integral:

\[ A_\mathrm{shape} = \iint f(x,y) \, dx \, dy \]

\noindent
where $f$ is the definition of the shape:

\[ f(x,y) = \left \{ \begin{array}{ll}
1, \, \, \text{in shape} \\
0, \, \, \text{else}
\end{array} \right.  \]

\noindent
This would certainly be a very tricky integral; first, we'd have to figure out a functional form for $f(x,y)$.
Then, we'd have to hope to come up with a strategy to perform the two-dimensional integral.

Instead, what we could do is draw a rectangular box around the odd shape; the ratio of the two areas would be

\[ r = \frac{A_\mathrm{shape}}{A_\mathrm{box}} \]

\noindent
where the area of the box is easily known (\textit{e.g.}, length $\times$ width). 
Then, we can say the area of the shape is just $A_\mathrm{shape} = r A_\mathrm{box}$, and if we can come up with $r$, we have the area of the shape.

In simple Monte Carlo integration, we overcome this problem by randomly sampling points in the $x,y$ domain\footnote{From a uniform distribution of course; that is, where every point in the box is equally likely.} of the box to build this ratio.
The more points we draw, the better our estimate of $r$ becomes.
(We can think of this as randomly throwing darts towards the box, and counting where they land.)

\[
r \approx \frac{N_\text{inside}}{N_\text{total}}
\]

\noindent
where the two $N$'s are the number of random points inside the shape (that is, where $f=1$), and the total number of random points generated, respectively.
Thus, if we can throw enough darts in a truly random fashion, we can avoid doing a messy integral; this is ``Monte Carlo integration''.

In practice, we also often use this when we could not write a simple analytical form for $f(x,y)$.
Imagine if to evaluate $f$ we had to perform an electronic structure calculation; \textit{e.g.}, we feed to our ``function'' the atomic coordinates $x$ and $y$ of a system and $f$ tells us if the system is magnetic.
In this case, we would have no choice but to use some sampling method, as we would not be able to write out an analytical integral to solve.
In this case, MC provides a more efficient means of unbiased sampling than just making a grid of $x,y$ data points.
In the next section, we'll instead discuss how to efficiently perform \textit{biased} sampling.


\subsection{The Metropolis algorithm (biased sampling)}

As an aside, we'll present the original development of the Metropolis algorithm; you can feel free to skip to the next section if you are not interested in its origins and how this leads to sampling from within a distribution.
The Metropolis algorithm was developed  at Los Alamos National Laboratory in the postwar era.
This algorithm was developed to predict chemical and material properties (phases and equations of state) using their newly developed ``fast computing machines''.
In the basic scheme, we are looking to compute an average value of some property $F$; i.e., $\langle F \rangle$; in the example we'll discuss below this might be the average bond length in a molecule.
We will follow closely the original logic of Metropolis~\etal~\cite{Metropolis1953}.
From statistical mechanics, the average value of some property we are trying to predict can come from a probability-weighted integral, as in:

\[
\left\langle F \right \rangle = 
\int F(\vec{x}) \, P(\vec{x}) d^{3N} \vec{x}
\]

\noindent here $\vec{x}$ is the vector of all atomic positions.\footnote{Note I have taken momentum ($\vec{p}$) out of this integral for simplicity; this is discussed in the original Metropolis paper but can normally be safely ignored.  (Roughly, this is due to the assumption that $F$ is only a function of position $\vec{x}$, and not momentum, and thus the momentum terms factor out equivalently from the numerator and denominator.)}
$P$ is the probability of observing the atomic configuration $\vec{x}$, which is determined from from the Boltzmann distribution:

\[ P(E(\vec{x})) = \frac{e^{-E(\vec{x})/\kb T} d^{3N} \vec{x}}
	{\int e^{-E(\vec{x})/\kb T} d^{3N} \vec{x}}
\]

\noindent
That is, the probability is a function of the energy of the configuration.
The integrals in both of the above equations are over all of the $3N$ positions, which makes them quite difficult to solve.

For perhaps the simplest concrete example, consider a simulation with just two H atoms where we want to know the average bond length between the two atoms.
This thought-experiment illustrates some important points: we need to weight the sampling in order to bias it towards low-energy configurations where there is a bond.
Also, if we sampled in an unbiased manner we would spend most of our time with the H's far apart, whereas we know from chemical intuition that the H's should spend most of their time reasonably close together.

A na{\"i}ve approach would put the particles in all possible positions $\vec{x}$, calculate their energy, and weight each sample with an appropriate Boltzmann term; however, even a reasonably course grid would be very expensive (``the curse of dimensionality'') and may miss some important points.
We could also think of populating our integrals by drawing randomly from a uniform distribution as in Monte Carlo Integration example above.
In principle this would work: we would multiply each point we sampled by its Boltzmann term (proportional to probability) to build the fraction.
However, we would waste a lot of time sampling improbable configurations that don't contribute much to the property; \textit{i.e.}, high energy configurations have a small Boltzmann term, and thus every high-energy configuration adds $\sim$0 to \textit{both} the numerator and denominator.

The insight of the Metropolis algorithm is that instead of sampling from a uniform distribution and multiplying by Boltzmann weights before summing, we could instead sample directly from the Boltzmann distribution and add together \textit{unweighted} terms.
This is mathematically equivalent, and intuitively this means we will preferentially sample the high-probability terms (low energy), which are the most important ones.

The general algorithm is:
{
\newcommand{\q}[1]{\ensuremath{\v{x}^{[#1]}}}

\begin{enumerate}
\item  Choose an initial configuration of atoms, \q{0}. Calculate its energy, $E(\q{0})$, such as from an electronic structure or interatomic potential method. The probability of the existence of this state is then \textit{proportional to} the Boltzmann factor

\[ P(\q{0}) \propto \exp \left\{ \frac{-E(\q{0})}{\kb T} \right\} \]

(The exact probability is this factor divided by the sum of the Boltzmann factor of \textit{all} configurations\footnote{This sum is called the ``partition function'' and is the central abstraction of statistical mechanics.}---which is a very difficult thing to calculate.)

Add this initial configuration to the \emph{ensemble}, or collection, of states.

\item
Propose a random-walk step; such a random walk is known as a \emph{Markov chain}.
That is, for each degree of freedom (coordinate) $i$ in $\vec{x}$, move it according to

\[ x^{*}_i = x^{[j]}_i + \Delta x_i^\mathrm{max} \cdot \xi_i \]

where $\xi_i$ is a randomly-drawn number, one per coordinate, from a uniform distribution between -1 and +1.
$\Delta x_i^\mathrm{max}$  is the maximum allowed step size for coordinate $i$.
The star ($*$) here just indicates the step has been proposed, and not yet added to the ensemble.
Again, calculate this ensemble's energy, and note that its probability is proportional to the Boltzmann factor:

\[ P(\v{x}^{*}) \propto \exp \left\{ \frac{-E(\v{x}^{*})}{\kb T} \right\} \]

 \item The ratio of the probability of the new step to that of the old step is 

\[ r = \frac{ P(\v{x}^{*})}{ P(\q{j})} =  \exp \left\{ \frac{-(E(\v{x}^{*}) - E(\q{j}))}{\kb T} \right\} \]

If $r>1$, accept the step.
Otherwise, accept the proposed step with a probability $r$.
That is, if the new step is lower in energy than the previous step, always accept it.
If not, draw another random number between 0 and 1, and if this random number is less than $r$ accept the proposed step.

If the proposed step is accepted, add it to the ensemble and take the next random-walk step from this point; that is, set $\q{j+1} = \v{x}^{*}$.

In either case (whether the step is accepted or rejected), return to step \#2 to continue the algorithm.


\end{enumerate}

At the end of the simulation, we have a Boltzmann-weighted \emph{ensemble} of configurations, and the average of any property value can be calculated as the average of the individual values in this ensemble.
Note that in order to assess the true probability of any configuration, we would have to calculate the denominator (that is, the partition function), which would require a knowledge of all points in space, and is extraordinarily expensive.
This method avoids that by working with only ratios of probability (in step \#3), such that the partition function never needs to be calculated.
Thus, anything that is proportional to probability will do.

\paragraph{Proof that this leads to a Boltzmann distribution.}
We will follow Metropolis \etal's logic.
Consider two states of our system, $u$ and $s$, which can be connected by a step on the Markov chain; $u$ and $s$ are unique atomic configurations.
The probability of \emph{proposing} a step from $u$ to $s$ or vice versa is equal and is given by $P_{us} = P_{su}$.
Here we assume a discrete set of possible states (a lattice of possible positions) for simplicity, but note that this proof holds also in continuous space.
Suppose we have run our simulation for some time and we have an ensemble of states, and $\nu_u$ and $\nu_s$ are the number of ensemble elements in states $u$ and $s$, respectively.

Assume that $s$ is the lower energy configuration, $E_\mathrm{u} > E_\mathrm{s}$.
(u and s are relatively unstable and stable states.)
Then the number of systems that would be expected to move from $u$ to $s$ is 

\[ u \rightarrow s: \hspace{2em} \nu_u P_{us} \cdot 1 \]

\noindent because $s$ is the lower energy state, these steps are accepted with 100\%\ probability, which we have indicated by explicitly multiplying by one.
The number of steps that would be expected to move the other way is

\[s \rightarrow u: \hspace{2em} \nu_s P_{us} \exp \left \{ \frac{-(E_u - E_s)}{\kb T} \right\}
\]

\noindent Thus, the \textit{net} steps from $u$ to $s$ is

\[ \mathrm{net}(u \rightarrow s): \hspace{2em} \nu_u P_{us} - \nu_s P_{us} \exp \left \{ \frac{-(E_u - E_s)}{\kb T} \right\}
\]

\noindent
So, the condition in which we would expect more states to move from $u$ to $s$---that is, when the term above is positive---is given by:

\[  \nu_u  > \nu_s \exp \left \{ \frac{-(E_u - E_s)}{\kb T} \right\} \]

\[  \frac{\nu_u}{\nu_s}  > \exp \left \{ \frac{-(E_u - E_s)}{\kb T} \right\} \]

\noindent That is, if the number of states in the relatively unstable state $u$ relative to the number of states in $s$ exceeds the Boltzmann ratio, then we would expect a net conversion of $u$ to $s$, and vice versa.
Thus, this sampling pattern will tend toward the Boltzmann distribution.

}


\section{Markov Chain Monte Carlo\label{sxn:mcmc}}

The computational workhorse of Bayesian methods are known as Markov-Chain--Monte-Carlo (MCMC) methods---the basic variety of which is the Metropolis algorithm described above.
(There are also other methods of generating the ensemble, but we'll constrain our discussion.)
First, let's define the terms:


\begin{itemize}
\item Monte Carlo: just means random numbers are involved. Named after the gambling city--state.
\item Markov Chain: also known as a ``drunken walk''. This is the way of generating random numbers, where we start at a point and take steps in random directions / magnitudes. In the long-run, it does not remember history. That is, the probability of each event depends only on the previous event, and not how the previous event was reached.
\end{itemize}

\paragraph{The Metropolis algorithm.}
Here we describe how the Metropolis algorithm is used in parameter estimation; the mechanics of the method are identical to the atomistics method described above.
Imagine that we are fitting a model of the form of equation~\eqref{eq:bayes-model}:

\[ \ym_i = f(\v{x}_i; \v{\theta}) + \varepsilon_i \]

\noindent
where the error is drawn from a normal distribution with width $\sigma$; that is, $ \varepsilon \sim \mathcal{N}(0, \sigma)$.
The goal is to build an \emph{ensemble} of values of these parameters, ($\v{\theta}$ and $\sigma$) where the frequency of their occurrence in the ensemble is in proportion to their probability of being the true parameters.
The algorithm goes:

\begin{enumerate}
\item
Choose initial values for $\v{\theta}^{[0]}, \sigma^{[0]}$.
Add this collection of parameters as the first point to the ``ensemble'', which will be our collection of reasonable answers.
That is, you now have $(\v{\theta}^{[k]}, \sigma^{[k]})$ for $k=0$, and will add terms to this for $k=1, 2, 3, \cdots$.

\item
Propose new values via a random walk (Markov chain) step, such as
\[\theta_j^{*} = \theta_j^{[k]} + \Delta \theta_j^\mathrm{max} \cdot \xi_j \]
where $\xi_j$ is a randomly drawn number, for example from a uniform distribution on (-1, +1).
The $*$ means this step is only proposed, not accepted.
$\Delta \theta_i^\mathrm{max}$ is a user-defined parameter.
Do this for all $\theta_j$ in $\v{\theta} = [\theta_j]$ as well as for $\sigma$, drawing a unique random number for each.
(For example, if \v{\theta} has two elements, you would draw three unique random numbers, one each for $\theta_1$, $\theta_2$, and $\sigma$ and produce proposed parameters $\theta_1^*$, $\theta_2^*$, and $\sigma^*$.)

\item
The ratio of the probability of the new step to the old step is
\[ r = \frac{P(\v{\theta}^{*})}{P(\v{\theta}^{[k]})} = \frac{\pcm{\vym}{\v{\theta}^{*}, \sigma^{*}} \pum{\v{\theta}^{*}, \sigma^{*}}}{\pcm{\vym}{\v{\theta}^{[k]}, \sigma^{[k]}} \pum{\v{\theta}^{[k]}, \sigma^{[k]}}} \]

We decide whether to accept or reject based on $r$.
We first draw a new random number between 0 and 1 called $d$, and

\begin{itemize}
\item if $r \ge d$, we accept the proposed step. That is, we set $\v{\theta}^{[k+1]} = \theta^*$ and $\sigma^{[k+1]} = \sigma^*$ (and increment $k$).
\item if $r < d$, we reject the proposed step, and do not add it to our ensemble or increment $k$.
\end{itemize}

In either case, we return to Step \#2 and iterate until we are satisfied that we have a large enough ensemble.

\end{enumerate}

Note that when $r > 1$, this means the proposed parameters values are more likely than the existing parameter values at step $k$.
You can also see we will \emph{always} accept these more likely values, as $d$ cannot be greater than one.
When $r < 1$, the proposed parameters are less likely, but we still accept this proposed step with a probability $r$.

It can be shown (see the earlier discussion of the Metropolis algorithm) that over time this procedure samples from the desired probability distribution.
That is the values of $\v{\theta}$ and $\sigma$ will be representative values from the population of most probable values.
Note that this method allowed us to avoid the tricky integral of equation~\eqref{eq:bayes-tricky}; since we care about the probability \emph{ratio}, this denominator cancels out of the above routine.

\subsection{Some practicalities of the implementation}

\begin{enumerate}
\item
Note that the probabilities you calculate in this algorithm will be \emph{tiny} numbers, because they are multiplying many low probabilities together.
\textit{E.g.}, see equation~\eqref{eq:bayes-yobs}; it's not unusual to calculate probabilities on the order of $10^{-10000}$.
For this reason, in practice it's much better to take the logarithm of each of the equations when implementing this computationally.
\textit{E.g.}, that equation would become

		\[ \ln \pcm{\vym}{\v{\theta}, \sigma} = N \ln \left(\frac{1}{\sqrt{2 \pi} \, \sigma}\right) - \frac{1}{2} \sum_{i=1}^N    \frac{ \left( \ym_i - f(\v{x}_i; \v{\theta}) \right)^2}{2 \sigma^2}  \]

\item
If there is a lot of data, the choice of the prior becomes unimportant; it gets washed out by the data.
For small data sets (and in the limit of no data!) the prior becomes important.
Thus, for large enough data sets you can be somewhat flexible on your choice of the prior distribution for your parameters.

\item
If you plot a ``trace'' of your accepted parameters versus step in the algorithm, you will probably see that it drifts at the beginning of the algorithm before settling into the high-probability region.
Many people establish a ``burn-in'' period---that is, they might throw away the first 1000 steps---to get rid of this initial drift and just start sampling once the algorithm has settled into a random walk.
This can reduce the total number of steps you need.

\end{enumerate}

\subsection{Packages}

As with most numerical methods, it's typically most efficient to use pre-packaged methods rather than try to build your own implementation, at least after you have understood the basics of how the sampling works.
There is a growing number of packages available that handle the MCMC, and related routines, for Bayesian inference.
One of the biggest in the python ecosystem is PyMC3.
I personally find model building in this code to be a bit confusing, and since its backend (Theano) has been discontinued it is uncertain that this will continue to be the preferred approach.
A simpler option is ``sampyl'', which is what we are using in our homework assignments.
Meanwhile, some of the big machine-learning packages, such as ``TensorFlow'', are also including MCMC algorithms.

\section{Propagating error with the ensemble}

Once you have created an \emph{ensemble} of possible model parameters, you can work directly with the ensemble in order to understand error propagation.
For example, after you perform linear regression with Bayesian inference, you will get an ensemble of (slope, intercept) pairs.
If you would like to make a new prediction of $y$ given a point $x$, you should make a prediction with each (slope, intercept) pair independently.
Then, you can look at the spread of the ensemble predictions of $y$ in order to understand the uncertainty of the prediction.
For example, you can make a 95\%\ confidence interval based on the spread of the predictions.
You can further propagate error in similar ways, using the ensemble.
You should think of each set of model predictions in your ensemble as making an independent model prediction, then pool at the very end in order to understand the variance.




\endgroup
