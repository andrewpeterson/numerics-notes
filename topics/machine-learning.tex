\begingroup
\chapter{Machine learning and data science}

Machine learning and data science are topical areas that are rapidly growing in prominence.
We'll just briefly touch on these topics to give a quick introduction to how they work.
That is, we'll introduce precisely one machine learning model (neural networks) and one data science approach (principle component analysis).
The idea is if you only know one method from each category, these are the ones to know.

\section{Machine learning}

Machine learning can mean a lot of different things, but my favorite definition is:

\begin{quote}
\emph{ A computer algorithm that gets better with more experience without being specifically programmed.}
\end{quote}

\noindent
The most common use of machine learning is to create a model that takes in data and makes some prediction about the data.
For now, just think of it is as a black box.
The inputs and outputs could be many different things, like:
\vspace{1em}

\noindent
\begin{tabular}{lll}
\hline \hline
Input & Output & Form \\
\hline
Picture & Is it a cat? (Y/N) & (0,1) \\
Email & Spam? (Y/N) & (0,1) \\
Handwritten character (picture) & Number & 10 (0,1) probabilities (of being each digit) \\
Atomic configuration & Electronic energy & a number \\
Market data & Future stock price & a number \\
Road data & Should I turn? & (0,1) \\
Basketball statistics & Game points spread & a number \\
\hline \hline
\end{tabular}
\vspace{1em}

The machine-learning model can be thought of as a \emph{function} that takes in data and outputs a number or numbers; so essentially it is a mathematical function, but we don't know in advance the physical form of it.
So the trick of machine learning is to give the model a lot of flexbility, then figure out how to get it make good predictions (even if we don't know why it is giving good predictions).

\subsection{Classification versus regression}

Machine learning modeling tasks are typically divided into classificaiton and regression.
Classification problems are of the sort ``is it a cat?'', while regression problems are of the  ``what is the stock price?'' sort.
We can think of classification problems as having discrete (typically binary) outcomes, while regression problems have continuous outcomes.
However, in machine-learning the classification problems are typically structured such that the model still returns a continuous value between 0 and 1, where we interpret how close we are to 0 or 1 as being the confidence in the prediction (although it's not a statistically rigorous confidence).
That is, if an ``is it a cat?'' model looks at a picture and returns 0.99, we can have high confidence that it's a cat.

Since in both cases the objective is to return a continuous number, many machine-leraning models can be used for either classification or regression.

\subsection{How does the model ``learn''?}

Before we worry about the exact functional form, let's discuss how to fit a model.
For now, you should just think of the model as being a mathematical function that has lots of adjustable parameters, and in ``learning'' (called training) our objective is to find values of those parameters that give outputs that we like.

Let's assume we are trying to build an ``is-it-a-cat?'' model, and we have lots of examples; \textit{i.e.}, lots of pictures, some of cats (labeled 1) and some of dogs (labeled 0), horses (0), and skunks (0).
Let's just assume that the model has lots of adjustable parameters (but a fixed architecture), such that if you change the parameters you get a different prediction.
Finding the best parameters is really just an optimization problem, as we covered earlier in \secref{sxn:regression}.

\[ \min_{\v{\theta}} (\text{``loss function''}) \]

\noindent where the loss function is something that has a high (positive) value when it makes poor predictions on the example data, and a low (positive) value when it makes good predictions and \v{\theta} are the free parameters.
What is a good loss function?
How about a sum of square residuals?

\[ \text{loss function} = \sum_i^\text{training data} (y_\mathrm{pred}(\text{image}_i, \v{\theta}) - y_{\mathrm{actual},i})^2 \]

Here, $y_{\mathrm{actual},i}$ is the label associated with each image $i$; that is, values of 0 or 1.
$y_\mathrm{pred}$ is our flexible function, which gives outputs between 0 and 1 when given each image.
So, as long as we have a model and some free parameters, we can just use a function like BFGS to minimize the loss function and have a machine learning model.
As we get more data, we just have the machine re-train so that it gets better and better.

For the example of a cat, we might have a particulra value of $\v{\theta}$ that gives 

\vspace{1em}

\noindent
\begin{tabular}{lrrr}
& $y_\mathrm{pred}(\mathrm{image}_i, \v{\theta})$ & $y_{\mathrm{actual},i}$ & square residual \\
\hline
Image 1 & 0.89 & 1 & 0.012 \\
Image 2 & 0.23 & 0 & 0.053  \\
\hline
Sum of square residuals & & & 0.065 \\
\end{tabular}

\vspace{1em}

\noindent
Thus, an optimizer could vary $\v{\theta}$ in order to  achieve a sufficiently low value of the sum of square residuals.

\subsection{Neural networks: perhaps the most common machine-learning model}

If you only know one machine-learning model, it should be the neural network; this is currently the most popular model due to its highly flexible nature and its scalability to large sets of training data.
It is structured after how neurons (were thought to) communicate in the nervous system.
The simplest form of a neural network, called a feed-forward neural network, is shown in Figure~\ref{fig:nam-screenshot}.
Here, the math is pretty simple: each ``node'' (or neuron) takes as input the output of all the nodes in the previous layer.
These inputs are linearly combined; that is, each is multiplied by a coefficient before they are summed.
(Analogous with a line, there is also an ``intercept'' term that's not shown graphically.)
Before outputting it's value, this sum is typically put through an ``activation function'', which can help give nonlinear responses.
In this case, the activation function is $\tanh$, which is shown in the inset.
This activation function has regions of no response, regions of linear response, and regions of upward- and downward- curvature.
Many other activation functions are used in practice, and many derivative neural network architectures are used as well.


\begin{figure}
\centering
\includegraphics[width=0.95 \textwidth]{f/nam-screenshot.png}
\caption{\label{fig:nam-screenshot} An example neural network structure.
}
\end{figure}



\subsection{Universal function approximators.}

Neural networks are powerful as they have been shown to be capable of replicating any arbitrary function, given enough parameterization and (noiseless) training data~\cite{Hornik1989}.
Thus, we can think of machine-learning models as function approximators, which figure out the function they are approximating as we feed it more data.


\paragraph{Demonstrations of logic in a neural network: approximating digital logic gates.}
As a demonstration that neural networks can replicate arbitrary functions, here we show how they can replicate digital logic gates.
Here, we'll take the activation function to be the sigmoid function, which looks similar to $\tanh$ but is bounded by 0 and 1:

\[ g(x) = \frac{1}{1 + e^{-x}} \]


\begin{itemize}
\item \textbf{OR gate.}
An OR gate takes two inputs and returns 1 (True) if \emph{either or both} input is 1 (True).
Here, we show that a simple one-node neural network can approximate this behavior.
Using the left-hand architecture of Figure~\ref{fig:nn-logic}, the parameters $w_1$, $w_2$, and $w_3$ are free for us to adjust to get any behavior we like.
If we adjust them so that they are
					 \[g(-10 + 20 x_1 + 20 x_2) \]

then we get the desired behavior, as shown below.

\begin{tabular}{lrrr}
\hline \hline
		  $x_1$ & $x_2$ & True result & NN prediction \\
\hline
		  0 & 0 & 0 & $g(-10) \approx 0$ \\
		  0 & 1 & 1 & $g(10) \approx 1$ \\
		  1 & 0 & 1 & $g(10) \approx 1$ \\
		  1 & 1 & 1 & $g(30) \approx 1$ \\
\hline \hline
\end{tabular}

Draw it; here is the equation for the single node.

\item \textbf{AND gate.}
An AND gate takes two inputs and returns 1 (True) only if \emph{both} inputs are 1 (True).
We can also do this with a single node, setting the values to:

\[g(-30 + 20 x_1 + 20 x_2) \]

\begin{tabular}{lrrr}
\hline \hline
		  $x_1$ & $x_2$ & True result & NN prediction \\
\hline
		  0 & 0 & 0 & $g(-30) \approx 0$ \\
		  0 & 1 & 0 & $g(-10) \approx 0$ \\
		  1 & 0 & 0 & $g(-10) \approx 0$ \\
		  1 & 1 & 1 & $g(10) \approx 1$ \\
\hline \hline
\end{tabular}


\item \textbf{XOR gate.}
	An XOR (``exclusive or'') gate takes two inputs and returns 1 (True) only if one input is 1 and the other is 0.
	To replicate this, we actually need a bit more complicated neural network architecture as shown in the right of Figure~\ref{fig:nn-logic}.
	We can do sow with the equations:

\begin{align*}
		  N_{1,1} &= g(-10 + 20 x_1 + 20 x_2) \\
		  N_{1,2} &= g(30 - 20 x_1 - 20 x_2) \\
		  N_{2,1} &= g(-30 + 20 x_1 + 20 x_2)
\end{align*}

which gives the intended results:

\begin{tabular}{lrrrrr}
\hline \hline
		  $x_1$ & $x_2$ & True result & $N_{1,1}$ & $N_{1,2}$ & $N_{2,1}$ (NN prediction) \\
\hline
		  0 & 0 & 0 & $g(-10) \approx 0$ & $g(30) \approx 1$ & $g(-10) \approx 0$ \\
		  0 & 1 & 1 & $g(10) \approx 1$ & $g(10) \approx 1$ & $g(10) \approx 1$ \\
		  1 & 0 & 1 & $g(10) \approx 1$ & $g(10) \approx 1$ & $g(10) \approx 1$ \\
		  1 & 1 & 0 & $g(30) \approx 1$ & $g(-10) \approx 0$ & $g(-10) \approx 0$ \\
\hline \hline
\end{tabular}


\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.9 \textwidth]{f/nn-logic/gates.pdf}
\caption{Logic gates.
\label{fig:nn-logic}
}
\end{figure}

\subsection{Pre-packaged software}

This field is changing rapidly, and major companies like Google and Facebook have open-sourced their internal machine-learning codes (TensorFlow and PyTorch, respectively).
It's generally a good idea to use these sorts of packages rather than code your own, as they will be faster and more feature-rich.

\section{Principal component analysis \label{sxn:pca}}

Principal component analysis (PCA) is a powerful technique in data science, and can help to make sense of large data sets.
It can also compress the data, extract the important features, and reduce noise in the data.
In my opinion, it's best to first demonstrate PCA before discussing it further.

\begin{figure}
\centering
\includegraphics[width=1.0 \textwidth]{f/eigenfaces/raw-faces.pdf}
\caption{100 pictures of celebrities' faces. Each is a grayscale image with 4096 pixels.
\label{fig:raw-faces}
}
\end{figure}

\subsection{Eigenfaces}

A famous example of PCA is the eigenfaces example; I have recreated this in python here.
Figure~\ref{fig:raw-faces} shows 100 pictures of celebrities' faces.
Each image is in grayscale and contains 4096 pixels; that is, it is 64$\times$64 pixels in size.
In our analysis, each image will be represented as a vector \v{x} which has 4096 elements; that is, the pixel intensity is just given for each pixel (row-by-row).

In PCA, we search for the common variance among these images.
Ultimately, we will get a set of ``stencils'' that we can use to re-create any image to arbitary fidelity.
We can accomplish this with just a few lines of code; in python this is:

\begin{minted}{python}
sigma = 0.
for image in images:
    x = np.reshape(image, (image.size, 1))  # Convert to vector.
    sigma += np.matmul(x, x.T)
sigma /= len(images) - 1
w, v = np.linalg.eig(sigma)
\end{minted}

\noindent
The full code for this example is included at the end of this topic, and we'll discuss the math further in a subsequent section.
Briefly, we are creating the covariance matrix (\mintinline{python}{sigma}) and then finding it's eigenvalues and eigenvectors.

\begin{figure}
\centering
\includegraphics[width=1.0 \textwidth]{f/eigenfaces/pcs.pdf}
\caption{The first 20 principal components.
\label{fig:pcs}
}
\end{figure}

The eigenvectors are the principal components, and the eigenvalues are the importance of each one.
This will find 4096 principal components (because in this example our original vectors each had 4096 elements); however, we can rank order them by the magnitude of their eigenvalue.
The 20 most important principal components are shown in Figure~\ref{fig:pcs}.
We can think of these as stencils that we will later use to re-create our original images.

\begin{figure}
\centering
\includegraphics[width=1.0 \textwidth]{f/eigenfaces/cumsum.pdf}
\caption{Cumulative sum of the eigenvalues of each principal component. We can interpret this as how much of the variation in the original data is accounted for by including $x$ number of princiapl components.
\label{fig:cumsum}
}
\end{figure}

The eigenvalues give the relative importance of each principal component.
We plot the cumulative sum of the eigenvalues in Figure~\ref{fig:cumsum}.
Here, we can see that we have hit 1.0 by about the 100th principal component; meaning we can capture $\sim$100\%\ of the variation between images with 100 ``stencils'' or principal components.
We are already at about 80\%\ of the variation with just 20 principal components, and 90\%\ with about 40 principal components.

Next, we'll show how we can re-create any of our original images using these principal components as stencils.
The coefficient corresponding to each principal component is found just by taking the dot product of the original image (expressed as a vector \v{x}) with the desired principal component.
In python, this is simply:

\begin{minted}{python}
coefficients = np.matmul(image, v)
\end{minted}

\noindent
which gives us back the coefficients corresponding to all 4096 principal components.
(\mintinline{python}{v} is a matrix containing the principal components as columns; \mintinline{python}{image} is a column vector containing the pixel intensities of the image in question.)


\begin{figure}
\centering
\includegraphics[width=1.0 \textwidth]{f/eigenfaces/recreate-0.pdf}
\includegraphics[width=1.0 \textwidth]{f/eigenfaces/recreate-9.pdf}
\caption{Re-created images with various numbers of principal components. The number above each image indicates how many principal components were used in the re-creation.
\label{fig:recreated}
}
\end{figure}

We found the coefficients for two of the example images, and showed how we can re-create the images to varying degrees of fidelity in Figure~\ref{fig:recreated}.
We see that with about 20 coefficients we are able to recreate the images so that the face is starting to be recognizable; by 50 coefficients it is very clear which face it is, and by about 100 coefficients the quality is indistinguishable from the original images.
This is a neat trick; it takes only about 50--100 numbers to re-create the original images, which were originally expressed with 4096 numbers.


\subsection{Two-dimensional example}

The eigenfaces example above was in $\mathbb{R}^{4096}$ space!
That is, each of the 4096 pixels is considered a dimension (or a variable).
To simplify the understanding, let's look at something in two-dimensional ($\mathbb{R}^2$) space.
Figure~\ref{fig:pca-2d} shows a number of points in 2-d space, where the (untransformed) variables are named $x_1$ and $x_2$.

\begin{figure}
\centering
\includegraphics[width=0.45 \textwidth]{f/pca/drawing.pdf}
\caption{\label{fig:pca-2d} Principal components in two dimensions.
}
\end{figure}

Here, the principal components of the data are sketched on the figure as PC$_1$ and PC$_2$.
These can be thought of as new dimensions that are more suited to describing the data.
Note that they have their origins at the mean of the data.
Here, the first principal component (PC$_1$) is the vector that can ``explain'' the greatest amount of variation in the data.
In other words, if I only tell you the position of a data point on the first principal compenent, then the point on the PC$_1$ is reasonably close to the true point; if I also tell you $PC_1$ we recover the original data point, but we got the majority of the information from just knowing PC$_1$.

The second principal component is orthogonal (at a right angle) to the first principal component, and describes the remainder of the variation.

We can describe in words how the principal components are found for the two dimensional example; but in practice we use the matrix formulation of the next section.
The first principal component is the line that passes through the mean of the data, which minimizes the sum of square residuals of the line; that is, it's just the linear-regression line.
To find the second principal component,\footnote{Of course, since we know the principal components are orthogonal, we already know the second principal component in this example, but we describe how it is done to explain the case for many dimensions.} we next subtract out the variation explained by the first principal component and repeat the process.

\subsection{General procedure}

As with most numerical methods, there are good software packages that can accomplish PCA for you; in python the most prevalent is probably scikit-learn.
However, the methodology is quite simple; this is the covariance method.
If $\v{y}^{(i)}$ is an individual data point; \textit{e.g.}, an image, a gene expression, or a point on our plot earlier:

\begin{enumerate}

\item
Normalize each element of the vector\footnote{Note that in the eigenfaces example, we cheated a bit and offset the pixels by the mean of all pixels, rather than doing so pixel-by-pixel.} to have mean $\sim$0 and standard deviation $\sim$1 across the data set.
That is, map $\v{y}^{(i)} \rightarrow \v{x}^{(i)}$.

\item Calculate the covariance matrix \m{\Sigma} as:

\[ \m{\Sigma} = \frac{1}{n-1} \sum_{i=1}^{n} \v{x}^{(i)} \v{x}^{(i)\mathrm{T}} \]

Note that $\v{x}^{(i)}$ is a $p \times 1$ vector so this gives a $p \times p$ matrix, where $p$ is the number of variables.

\item
Find the eigenvalues and eigenvectors.
The eigenvectors are the principal componennts, and the eigenvalues are the rankings of these.

\end{enumerate}

\subsection{Eigenfaces python code}

The code to do the eigenface example is contained in two scripts below.
The first runs the PCA (which can take a few minutes to solve the eigenvalue problem) and the senond analyzes it.
Note that the first script should be named ``pca.py'' such that the second script can import from it.
The pictures themselves are the ``LFWcrop'' dataset, whose current URL is below; should that URL not work, the images are also contained within the repository for this document (discussed in the Preface).

\url{http://conradsanderson.id.au/lfwcrop/}

\pcode{f/eigenfaces/pca.py}
\pcode{f/eigenfaces/analyze.py}

\endgroup
