\begingroup

\chapter{Optimization of continuous functions\label{sxn:opt}}

% Augmented matrix.
\newenvironment{amatrix}[1]{ \left[\begin{array}{@{}*{#1}{r}|r@{}} }{ \end{array}\right] }
% Partial differential.
\newcommand{\pd}[3]{\left(\frac{\partial #1}{\partial #2}\right)_{#3}}
% Norm.
\newcommand{\norm}[1]{\left\lVert #1 \right\rVert}
% Minimize with underscore.
\newcommand{\minn}[1]{\underset{#1}{\mathrm{minimize}}}
% Shortcuts
\newcommand{\xk}{x^{(k)}}


\section{Overview\label{sxn:opt-key-points}}

\reading{B5, K22}

\noindent
Optimization is incredibly common in engineering and physics, and is perhaps the most commonly used numerical method in day-to-day use.
Optimization is useful in obvious ways: like optimizing devices or processes or economics, but also in many other numerical tasks, like finding the best-fit parameters for a model or finding a converged solution to a modeled problem.
In this section, we'll discuss optimization of continuous functions; the next section will cover graph theory in which optimization involving discrete variables is covered.

\subsection{Key points of optimization}

We'll first hit the most salient points:


\begin{enumerate}
\item \emph{You can only optimize one attribute.}
This is perhaps the most important point of optimization; that only one aspect of your system can be optimal.
In business, you can't simultaneously maximize earnings \emph{and} optimize worker happiness \emph{and} optimize environmental performance \emph{and} optimize customer satisfaction.\footnote{Fortunately for this example, no business knows perfectly how those factors influence earnings, so they might say that they want to maximize quality in order to maximize customer satisfaction, which will lead to more long-term sales, assuming these things are correlated. However, it's unlikely that this will \emph{also} improve environmental performance or worker happiness. This is the economic justification for regulations, which put prices or constraints on these factors so that they are included when businesses optimize their economics.}

\begin{figure}
\centering
\includegraphics[width=1.0 \textwidth]{f/optimization/drawing.pdf}
\caption{\label{fig:optimization}
	(Left) If we have two quantities, $f_1$ and $f_2$, that we want to optimize as a function of $x$, we cannot find one value of $x$ that optimizes both. We can, however, weight and add them, and look for an optimum of the combined function. (Right) A two dimensional contour map showing a loss function with many local minima.
}
\end{figure}

In Figure~\ref{fig:optimization}, we can see the problem graphically: there is not a value of $x$ that gives both a minimum in $f_1$ and in $f_2$.
Note that if we want to optimize in these two variables we need to combine them into a single function, like

\[ f = f_1 + c f_2 \]

Of course, the choice of $c$ is typically arbitrary---for example, $f_1$ and $f_2$ might not even have the same units---and will greatly affect what outcome we get.
Note something else we see here: there are multiple local minima; this is incredibly common in optimization.
Note also that the location of the global minimum will change depending on our choice of $c$.

\item \emph{Loss functions in general have many local minima, which are ``easy'' to find.}
As we just saw, it's common to have many local minima; we also show a higher-dimensional version of this in Figure~\ref{fig:optimization}.
We can typically find the local minimum of a function by ``walking downhill'', which is the basis of most methods.

\item \emph{The global minimum can be hard to find.}
Clearly, which local minimum we hit will depend on where we start walking from, and thus finding a local minimum is relatively easy.
However, finding the global minimum is typically much harder.

\item \emph{Trade-offs are optimized by ``Pareto'' optimality.}
If we can operate at the ``Pareto frontier'', we are optimizing the trade-offs.
This is discussed further in \secref{sxn:pareto}.

\item \emph{Optimization and NLAE are highly related.}
For continuous functions, optimization and systems of nonlinear algebraic equations (NLAE) mathematically reduce to the same problem.
(This is why \verb'fsolve' is in \verb'scipy.optimize'!)
Most of what we covered in the NLAE section can be directly adapted to optimization.
\end{enumerate}


\subsection{Problem formulation}
The (single!) function that we optimize is typically\footnote{Other common names included \defn{loss function} and \defn{objective function}.} called the \defn{cost function}.
Since we all want to pay the lowest cost, we conventionally describe optimization as a minimization problem:

\[ \minn{\v{x}} f (\v{x}) \]

\noindent
If you'd instead like to maximize a function, just multiply it by $-1$, or take its inverse, as appropriate, to convert it into a minimization problem.
Note that although we can only optimize one variable $f$, it can depend on as many independent variables as we like, \v{x}.
This is indicated by the variable \v{x} that appears under the word ``minimize''.

\subsection{Pareto optimality\label{sxn:pareto}}

Recall that the most important qualitative point of optimization is that you have to pick which \emph{one} attribute of your system you would like to optimize (\secref{sxn:opt-key-points}).
However, if you are trying to understand the \emph{trade-off} between various attributes, then people speak of Pareto-optimal solutions.

A common trade-off in engineering design is capital cost versus operating cost---for example, how much it costs to build a power plant versus the cost of electricity produced by that power plant.
See Figure~\ref{fig:pareto}.
You come up with ``generation \#1'' of your power plant, which has a certain level of capital and operating costs.
Then, you find great ways to improve on it that lower \emph{both} the operating cost and the capital cost.
Great.
You keep iterating and coming up with better and better solutions---but eventually you find that you can't lower the capital cost without increasing the operating cost, and vice versa.
This is the condition of Pareto optimality.

\begin{figure}
\centering
\includegraphics[width=0.45 \textwidth]{f/pareto/drawing.pdf}
\caption{\label{fig:pareto} Understanding trade-offs in the design of an engineering facility.}
\end{figure}

This is shown as the Pareto-optimal solution in Figure~\ref{fig:pareto}.
You can interpret any point along this curve in either of two ways: (1) for a given capital cost, if I am on the Pareto optimal curve I cannot further lower my operating cost, or (2) for a given operating cost, on the Pareto optimal curve I cannot further lower my capital cost.

This is an important point in real optimization schemes.
When you care about multiple attributes, your ultimate design should be on the Pareto optimal curve (on the ``Pareto frontier''); that is, if you are any where else, you could be doing better, even taking into account trade-offs.
Once you are on the Pareto frontier, then its a matter of judgement: how much do you prize one thing versus another?

You can imagine this is very useful in a business setting---if you can clearly lay out the trade-offs to your management, who will ultimately need to make the difficult decisions, then they have all the information they need to make informed decisions.

At its most basic, you can construct a Pareto optimality curve by just optimizing under constraints (\secref{sxn:constrained-opt}).
That is, if you are analyzing the trade-off between attributes A and B, optimize B under the constraint that A is fixed, and repeat this for various (fixed) values of A.
This will give you the Pareto-optimal curve, which you can interpret in either way.
Of course, the above approach can work only for the optimization of simple functions; we leave the optimization of more complex functions to their respective specialized fields.
Of course, there are may advanced and sophisticated algorithms for understanding Pareto optimality.

\section{Gradient-based methods}

\reading{B5:216}

\noindent
Mathematically, a necessary (but not sufficient) condition to state that we are at a local minimum is that the gradient is zero:

\[ \v{\nabla} f = 0 \]

\noindent
This is not sufficient to say we are at a minimum in $f$---we could be at any stationary point\footnote{Some mathematicians call these ``critical points'', but we use that term in the physical sciences for something else.}, which includes local maxima and other more complex features, such as saddle points.
However, since most techniques we'll discuss involve ``walking downhill'', it's likely that the stationary point we'll hit will be a local minimum.\footnote{Local maxima can be ruled out; in some very complicated cost functions there is some non-zero chance of landing at a saddle point, but this is unusual.}
Also, note the above equation applies equally at local minima or the global minimum, so the techniques we'll discuss here only apply to finding nearby local minima.

This gradient is a system of equations:

\[ \pd{f}{x_1}{} = 0 \]
\[ \pd{f}{x_2}{} = 0 \]
\[ \vdots \]

\noindent
That is, this is just a system of nonlinear algebraic equations as we discussed earlier in Section~\secref{sxn:nlae}, and we can use or adapt the methods from this section.
To simplify the notation a bit, we often just refer to the gradient of $f$ as $\v{\gamma}$:

\[ \v{\gamma} \equiv \v{\nabla} f = \v{0} \]


\subsection{General step.}
If we walk downhill we will be going towards a minimum.\footnote{Or negative infinity; but then you probably didn't formulate your problem well.}
Gradient-based methods start from a guess vector $\v{x}^{[0]}$ and take downhill steps.
We can define a general step like:

\[ \v{x}^{[k+1]} = \v{x}^{[k]} + a^{[k]} \v{p}^{[k]} \]

\noindent
Here, the superscripts like $^{[k]}$ refer to the value at the $k^\mathrm{th}$ step.
The direction of the step need not be the steepest way down the hill, and it can be better in general to choose something that is not the steepest descent.
Here, $\v{p}^{[k]}$ is a vector pointing downhill (mathematically, $\v{p}\T \v{\gamma} < 0$, where $\v{\gamma}$ is the true gradient of $f$), and $\alpha$ is the step size.

That is, we need to choose the step size and step direction, and we have an algorithm.

\subsection{Search direction.}

Let's discuss how to choose which direction we should move in.

\paragraph{Steepest descent.}
The most obvious choice is to just go straight downhill; this is called the ``steepest descent'' algorithm.
That is,

\[ \v{p}^{[k]} = -\v{\gamma}^{[k]} \]

\noindent
(and $\alpha^{[k]}$ may be some user-defined parameter.)

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/conjugate-gradient/drawing.pdf}
\caption{\label{fig:conjugate-gradient}
The trouble with taking steps straight downhill.
}
\end{figure}

\noindent
Figure~\ref{fig:conjugate-gradient} shows the issue with this---we can often bounce around on the cost function surface, and the convergence can be rather slow and erratic.

\paragraph{Conjugate gradient.}
One solution to the bouncing-around problem is to ``mix'' in  some of the last step with the next step, as in

\[ \v{p}^{[k]} = -\v{\gamma}^{[k]} + \beta \, \v{p}^{[k-1]} \]

\noindent
If you work out what this would do to the second step in Figure~\ref{fig:conjugate-gradient}, you can see that we would cancel out some of the bouncing, while keeping most of the direction we would like to go in.
$\beta$ need not be defined by the user, but instead can be chosen automatically via a couple of different formulations.
\textit{E.g.}, Fletcher--Reeves:

\[ \beta = \frac{\v{\gamma}^{[k]} \cdot \v{\gamma}^{[k]}}{ \v{\gamma}^{[k-1]} \cdot \v{\gamma}^{[k-1]}} \]

\noindent
or Polak--Ribiere,
\[ \beta = \frac{\v{\gamma}^{[k]} \cdot (\v{\gamma}^{[k]} - \v{\gamma}^{[k-1]})}{ \v{\gamma}^{[k-1]} \cdot \v{\gamma}^{[k-1]}} \]

\noindent where the latter is generally considered better.
If you choose 'CG' in scipy.optimize.minimize, you will get Polak-Ribiere.

\subsection{Step size \label{sxn:opt-stepsize}}

\reading{B5:216}

\noindent
We also discussed linesearch in NLAE, let's look at that in more depth here.
Once we have a good search direction, perhaps we can now perform a one-dimensional optimization?
Optimization in one dimension is easier, so this could be done to find the absolute minimum along this search direction.
If we do this, it is called a \emph{strong linesearch}.
However, since it's not so likely that the true minimum lies along this line, then this is kind of a waste and we're better off switching directions once we know we've started going downhill.

So typically this is done by a \emph{weak linesearch}, which we described earlier in NLAE.
That is, start with $a^{[k]} = a_\mathrm{max}$, if this does not decrease $F$, then 

\[a^{[k]} \leftarrow \frac{a^{[k]}}{2} \]

\noindent
This, specifically, is called backtrack linesearch.
There are also means for finding a good value of $a_\mathrm{max}$, and for making more stringent criteria than just a decrease in $f$.
See Beers pp.~213--217 if you want to know all those details.


\section{Hessian-based methods}

What we'll discuss here is entirely analogous to the discussion of the Jacobian matrix in NLAE.
However, here we use the term \emph{Hessian} matrix, because in an optimization problem we have a second, not a first, derivative.
But the mechanics are entirely the same.

First, let's remember that at our true solution, $\v{\gamma} = 0$, so at a perfect step size and direction $\v{p}$ we have $\v{\gamma}(\v{x}^{[k]} + \v{p}) = 0$.
How can we estimate the perfect step $\v{p}$?
When in doubt, use a Taylor series expansion:

\[ \v{\gamma}(\v{x}^{[k]} + \v{p})
= \v{\gamma}(\v{x}^{[k]}) + \m{H}(\v{x}^{[k]}) \v{p} + \cdots
\]

\noindent
where \m{H} is the Hessian matrix, which we can see must be defined as

\[ \m{H} = [H_{ij}] \]

\[ H_{ij} = \pd{\gamma_i}{x_j}{} = \left( \frac{\partial^2 f}{\partial x_i \partial x_j} \right) \]

\noindent
That is, it is the matrix that describes the curvature of the function $f$ at a certain point \v{x}.
So, since we said the left-hand side of the Taylor series was zero, we can re-write this as the linear system

\[ \m{H}^{[k]} \v{p}^{[k]} = - \v{\gamma}^{[k]} \]

So if we know \m{H}, we can make a really good guess of where we should take our next step.
(We can then use 1 as our $a_\mathrm{max}$ described earlier in a linesearch implementation.)

\paragraph{Hessian, the person.}
Otto Hesse was a German mathematician.
His PhD advisor was Carl Jacobi, of Jacobian matrix fame.
The Hessian is the matrix of second derivatives, and the Jacobian is the matrix of first derivatives.

\subsection{Approximate Hessians (Quasi-Newton)}

In practice, the Hessian is often not known or hard to calculate. (But if it is available, use it!), and so methods exist to generate an approximate form of \m{H} during the course of the optimization, building up an approximate Hessian which we'll call \m{B}.
For example, we might start with $\m{B}^{[0]} = \m{I}$, then update with

\[
		 \m{B}^{[k+1]} = 
		 \m{B}^{[k]} + \frac{\v{\Delta \gamma} \, \v{\Delta \gamma}^\mathrm{T}}{\v{\Delta \gamma}^\mathrm{T} \v{\Delta x}}
		 - \frac{\m{B}^{[k]} \v{\Delta x} \, \v{\Delta x}^\mathrm{T} \m{B}^\mathrm{[k]}}{\v{\Delta x}^\mathrm{T} \m{B}^{[k]} \v{\Delta x}}
		  \]

This is known as the Broyden--Fletcher--Goldfarb--Shanno (BFGS) algorithm~\cite{Broyden1970,Fletcher1970,Goldfarb1970,Shanno1970a}, which is probably the most popular optimizer and is considered the best general-purpose optimizer.
This is what \mintinline{python}{scipy.optimize.minimize} defaults to for normal problems.
(Note the delta terms are the difference in those vectors over the last two steps.)
There also limited memory versions of this, known as L-BFGS, which can be useful if you have a very large number of variables in \v{x}.


\section{Gradient-free methods}

Often, the gradient of the function $f$ to be minimized is unknown.
This may be because $f$ is a very complicated function (like the output of some other complicated algorithm), or perhaps because you don't want to take the effort (and risk) of deriving and coding the gradient of $f$.
In this case, you have two choices: (1) use a numeric approximation of the gradient, or (2) use a gradient-free method.

In the former case (using a numeric approximation of $\v{\nabla} \, f$), the gradient is typically approximated by perturbing each independent variable (that is, \v{x}) and observing the response in $f$; this is the same approach as in numeric integration (\secref{sxn:int}).
This is the approach used in scipy's BFGS implementation, for example.
The basic cost of this is an extra $n$ function calls per iteration, where $n$ is the number of elements in \v{x}.
This can lead to a large number of function calls in this process, so if the cost of the function call is high, it's typically worth examining whether this approximate gradient is worth it.
(That is, put an variable inside your loss function that tracks the number of times it is called; this can be as simple as adding a print statement.\footnote{In python, the easiest way to add an explicit counter do this is with a \emph{global} variable; you could also write your loss function as a class so that it can store variables.})
Additionally, some ``noisy'' functions are challenging to estimate the derivative to analytically; noisy functions can arise, for example, if the function itself is a complicated algorithm that is solved iteratively, there will inherently be some noise.

Therefore, it can sometimes be useful to employ a gradient-free method, which optimizes only based on the value of the function $f$ without trying to use the gradient in the process.
The most famous is the Nelder--Mead algorithm, which we describe below.
(This is the algorithm used in the \mintinline{python}{scipy.optimize.fmin} algorithm.)

\subsection{Nelder--Mead (downhill simplex)}

\reading{K22.3-22.4, B5:213}

\noindent
This algorithm relies on a ``simplex''; think of a simplex as a generalized triangle.
That is, a triangle is the smallest shape that fills space in two dimensions; it requires three points.
(A line segment of 2 points would be the smallest shape that fills space in 1 dimension, while a tetrahedron of 4 points is the smallest shape that fills space in 3 dimensions.
A simplex containts $n + 1$ points, where $n$ is the number of dimensions.

This algorithm is pretty intuitive; I like to think of it as flipping a rock down a hill.
Imagine you are in the mountains and you have a triangle-shaped rock sitting on the ground.
In this algorithm, you go to the point of the rock that is farthest up the hill and you flip it over, so that it flops a little bit down the hill.
This is the basic mental image I use to consider how this works; the real algorithm is described below and illustrated in Figure~\ref{nelder-mead}.

\begin{figure}
\centering
\includegraphics[width=0.33\textwidth]{f/nelder-mead/drawing.pdf}
\caption{\label{nelder-mead}
Nelder--Mead simplex descent algorithm.
}
\end{figure}

\paragraph{Algorithm.}
The algorithm is described very roughly here.
In your homework, you are implementing the version on wikipedia which provides more detailed in structions.


\begin{enumerate}
\item
Make a simplex; that is, a triangle of dimension $n+1$ where $n$ is the number of free variables.
Calculate $f$ at each of these points.

\item
Try ``flipping it'': move the highest-valued (in $f$) point $\v{x}_{n+1}$ to
\[ \v{x}_r = \v{x}_0 + \alpha (\v{x}_0 - \v{x}_{n+1}) \]
where $\v{x}_0$ is the centroid of the other points (the middle of the line, for a triangle) and $\alpha$ is a positive parameter, usually 1.
Keep the flipped version if $f(\v{x}_r)$ is less than any other $f(\v{x}_i)$ (except $n+1$, because then we're just going to flip back and forth).

\begin{itemize}
\item Also let the simplex expand. If the reflected point is the best new point, then try 
\[ \v{x}_e = \v{x}_0 + \gamma (\v{x}_r - \v{x}_0) \]
\end{itemize}

\item If $\v{x}_r$ is still the highest energy point in the simplex after flipping, instead put it back where it was and start shrinking the simplex from this point.
That is, when we get where the simplex surrounds the local minimum, we're going to start moving in the highest energy points one by one as we hone in on the true answer.
Specifically,
\[ \v{x}_c = \v{x}_0 + \rho (\v{x}_{n+1} - \v{x}_{0}) \]

$0 < \rho \le 0.5$.

\item If none of those improved it, shrink all points but the best with 

\[ \v{x}_i = \v{x}_1 + \sigma (\v{x}_i - \v{x}_1) \]

\end{enumerate}

\section{Non-linear regression: parameter estimation \label{sxn:regression}}

One very common use of optimization---including the \verb'minimize' functions of scipy---is in performing generalized regression.
Most of us are very familiar with performing linear regression---which you can easily do in a spreadsheet, for instance---in which you have some set of $x,y$ data for which you want the best-fit line.
Linear regression gives you the slope and intercept that minimize the error in the predicted line.

Regression to a nonlinear model is not really any harder, especially if you are working in a programming language like python or matlab.

First, you write a model.
You can use whatever physics you like (hopefully something relevant!) to create the  model.
The model can have as many independent and dependent variables as you like, and in general there is no limit on the number of (adjustable) parameters in the model.\footnote{We will leave the \emph{quality} of your model to your particular field of expertise. In most fields, having a lot of adjustable parameters indicates a poor understanding of the physics of the problem. A key exception is machine learning, where the philosophy is to instead throw your hands up in the air and embrace adjustable parameters...}
Let's say that our model is of the form:

\[ y^\mathrm{pred} = f(\v{x}; \v{P}) \]

\noindent
where $y^\mathrm{pred}$ is the model's output (dependent variable), \v{x} is the model's input (independent variables), and \v{P} are the parameters to be adjusted.
We assume that we have a series of measurements of \v{x} and corresponding measurements of $y$; each pair will be denoted as $(\v{x}_i, y_i)$.

Next, you construct a cost function whose purpose is to reduce the errors on the model prediction; you can construct this however you like.
The most common is to reduce the sum of square errors (SSR):

\[ \mathrm{SSR}(\v{P}) \equiv \sum_i^{N_\mathrm{measurements}} \left(y^\mathrm{pred}\left(\v{x}_i; \v{P}\right) - y_i\right)^2 \]

\noindent where $\{\v{x}_i, y_i\}$ is the set of data to fit.
The optimization task is then to minimize this loss function by varying $\v{P}$:

\[ \minn{\v{P}} \, \mathrm{SSR}(\v{P}) \]

Below, we show a small example of how this can be written in practice; this assumes your measured data is stored in \mintinline{python}{xs, ys}.

\begin{minted}[fontsize=\footnotesize]{python}
from scipy.optimize import minimize

def get_ypred(x, parameters):
    # Your model here!
    # E.g., for a line
    slope, intercept = parameters
    return slope * x + intercept

def get_loss(parameters):
    SSR = 0.
    for x, y in zip(xs, ys):
        SSR += (y - get\_ypred(x, parameters)**2
    return SSR

answer = minimize(get_loss, p0)
\end{minted}

Here, \mintinline{python}{p0} contains the initial guess of the parameters.
Of course, if you can calculate the derivative of the loss function you should\footnote{If your only concern is computational efficiency, you definitely should. However, you can argue that if your objective is getting to the \emph{right} solution, and your problem is cheap, you may not want to because this introduces a second place where you could have a bug in your code.} do that too, as it will make the optimization much more efficient.

\section{Constrained optimization\label{sxn:constrained-opt}}

Constrained optimization is inherently harder than unconstrained optimization, but it is very common.
Constraints can result from many conditions specific to your problem: \textit{e.g.}, mass cannot be negative, some emission level must be below a specified level, etc.
Here, we'll briefly describe three ways to deal with constraints, which depend upon your particular problem.

\subsection{Bounded variables}

The simplest case is if your \emph{dependent} variables have some bounds, which is conventionally written as:

\[ \begin{array}{rl}
\minn{\v{x}} & f(\v{x}) \\
\\
\mathrm{subject\, to\,} & \v{x}_\mathrm{min} \le \v{x} \le \v{x}_\mathrm{max} \\
\end{array} \]

\noindent
This situation is relatively straightforward; the optimizer just algorithmically does not allow the constrained variables to step outside of the given ranges.
(Actual algorithms are more sophisticated, of course, but we won't go through the details of how this is accomplished.)
For example, you can supply the \verb'bounds' keyword to scipy to do this.
The syntax is similar in other codes such as Matlab.

\subsection{Mathematically constrained\label{sxn:math-constrained}}

Alternatively, we can have mathematical constraints.
These problems are generally written as

\[ \begin{array}{rl}
		\minn{\v{x}} & f(\v{x}) \\
		\\
\text{subject to} &  g_i(\v{x}) = 0, i=1, 2, \cdots, n_i \\
		  & h_j(\v{x}) \ge 0, \,\,\, j=1,2,\cdots,n_j
\end{array} \]


\noindent
where some functions, in the form of equalities or inequalities, need to be satisfied.

See Figure~\ref{fig:constrained-optimization}, which illustrates the case for a constraint where we are minimizing a function $f$ along a constraint $g$.
I like to think of this as if you are hiking a path ($g$) which is along the edge of a valley, and you are wondering what the lowest altitude is that you will encounter on this path.
So $f$ gives the height of the land, but $g$ is the constraint as to where you can walk.

\begin{figure}
\centering
\includegraphics[width=0.3 \textwidth]{f/constrained-optimization/drawing.pdf}
\caption{\label{fig:constrained-optimization}
Constrained optimization. Think of $f$ as a contour map (the lowest point is in the center); and we are hiking along $g$ and looking for the lowest point we'll hike through.
}
\end{figure}


\paragraph{Adding terms to the loss function.}
Perhaps the easiest approach is to just add \emph{penalties} to your loss function that add to it when your constraint is violated.
For example, say you have a problem where you are trying to fit parameters to a model, as in \secref{sxn:regression}; that is, you are minimizing the SSR (sum of square residuals).
But let's also say we have a constraint; perhaps all the $x_i$'s in \v{x} have to sum to one.
(For example, if the $x_i$'s are chemical concentrations expressed in mole fractions, they need to sum to one.)
In this case, we can write this problem as

\[ \begin{array}{rl}
\minn{\v{x}} & \mathrm{SSR}(\v{x}) \\
\\
\text{subject to} & g(\v{x}) = \sum_i x_i - 1 = 0 
\end{array} \]


\noindent
Then, we can construct our loss function such that it includes this penalty.
For example, in python:

\begin{minted}[fontsize=\footnotesize]{python}
def get_loss(parameters):
    SSR = 0.
    for x, y in zip(xs, ys):
        SSR += (y - get_ypred(x, parameters)**2
    penalty = (np.sum(xs) - 1.)**2
    loss = SSR + c1 * penalty
    return loss
\end{minted}

You can use the constant \mintinline{python}{c1} to determine how much weight is put on reducing the residuals versus enforcing the constraint.
Note that although you can only optimize with respect to one variable, you can monitor multiple variables during the course of the optimization---that is, you can check the value of $g(x)$ when your minimization routine finishes, and if it's not as close to zero as you'd like, you can adjust your strategy (such as increasing \mintinline{python}{c1}.
An example of the combined decrease of $g$ and SSR, making up the loss function is shown in Figure~\ref{fig:combined-loss-function}.

\begin{figure}
\centering
\includegraphics[width=4.0in]{f/loss-function/drawing.pdf}
\caption{Example of a loss function that contains a built-in constraint. \label{fig:combined-loss-function}}
\end{figure}

%\begin{notyetready}

\paragraph{Lagrange multipliers.}
Lagrange multipliers are a well-known way to include constraints in optimization problems.
Since it's covered in ENGN 2010, we'll only briefly mention this topic here.

You can work out intuitively (Figure~\ref{fig:constrained-optimization}), or mathematically, that the condition where you hit a local minimum is when the two functions are tangent, which is written by

\newcommand{\vnabla}{\v{\nabla}}

\[ \v{\nabla} f = \lambda \vnabla g \text{\, (at optimum)} \]

\[ \mathcal{L}(\v{x}, \lambda) \equiv f(\v{x}) - \lambda g(\v{x}) \]

Note that this is now a function of an extra variable, $\lambda$.
If we take its gradient with respect to all the variables, we get

\[ \vnabla \mathcal{L} = 0 \]

\[ 0 = \pd{f}{x_i}{} - \lambda \pd{g}{x_i}{} \]

\noindent
(for all $i$) and

\[ g(\v{x}) = 0 \]

\noindent
So we recover our constraint, but we have a simple expression in $\mathcal{L}$.

This is an elegant technique, and is often useful in derivations.
Interestingly, the best definition I know of for temperature is that it is a Lagrange multiplier!
That is, if you maximize the entropy (\textit{i.e.}, probability) of a large system, under the constraint that it's total entropy is constant, you end up with an extra Lagrange multiplier $\beta$ that cannot be removed.
(We end up defining temperature as $\beta \equiv 1 / k_\mathrm{B} T$, where $k_\mathrm{B}$ is the Boltzmann constant.)
You can find a derivation of this in many textbooks, for example Dill \&\ Bromberg.~\cite{Dill2010}

\section{Financial optimization}

As mentioned in \secref{sxn:opt-key-points}, we can only fully optimize one attribute of our system; however, this attribute can combine various attributes.
That is, we can construct one loss function that contains all of the attributes that we care about, added together.
However, we can note as scientists and engineers that these system attributes often will not even have the same units.
However, in the real world, nearly all of these attributes will be converted into one common unit: money.

For example, we could imagine that we might want to minimize the amount of steel that is contained in an automobile that we are designing.
We are not doing this for the sake of conserving the world's iron ore resources, we are doing this because steel costs money.
(Or maybe because steel impacts some other attribute that we care about, like fuel economy---which we might want to optimize in order to increase sales and make more money.)
And if to reduce the amount of steel we need to add in some other, more expensive material like aluminum or a composite, we had better include the cost of that material in our optimization problem as well.



\subsection{Net present value}

If there's one concept from the world of finance that we as engineers should know, it's the net present value (NPV) analysis.
Generally speaking, in making decisions about systems that we are optimizing, our goal in the business world is to maximize the ``net present value'' of a project.
This is a relatively simple analysis where we look at all the future cash flows that we expect a project to result in, and calculate what they are worth to us today.

Let's present instructions for how to calculate the NPV:

\begin{enumerate}

\item
Determine the internal rate of return (IRR), also known as the discount rate.
This is typically a number around 8 to 20\% -- each company will set their own IRR, so this will often be a `given' in a problem.
Express this as $i$, such as $i = 0.08$ for 8\%.

\item For each year $t$, calculate the net cash flow, $R(t)$. This is simply
$$ R(t) = \sum \left(\text{cash in during year $t$}\right) 
- \sum \left(\text{cash out during year $t$}\right)
$$

(Or income/savings attributed to the project minus expenses/loss-of-sales attributed to the project for that year.)

\item Convert each year's cash flow into a present value (PV) by the formula

		  $$ \text{PV}(t) = \frac{R(t)}{(1 + i)^t} $$

\item Sum the PV's over the $N$ years of the project, often 20 or 30 years. Note that the PV's at the end of projects tend to be low\footnote{With notable exceptions. For instance, nuclear power plants have a large decommissioning cost in the last year of the project, which is the cost to tear down the power plant, an expensive job with nuclear reactors.}, so the choice of the project duration is not as crucial as you might think. This results in the `net present value' (NPV):

		  $$ \text{NPV} = \sum_{t=0}^{t=N} \text{PV}(t) $$
\end{enumerate}

\paragraph{Example 1.} Say a company with an IRR of 10\% ($i=0.1$) wants to understand the economics of changing its light bulbs from fluorescent to LEDs. Perform the analysis per-bulb. Assume the LED bulb will cost \$5 per bulb and the labor to replace the bulb costs \$4 per bulb. The annual electricity savings are estimated to be \$2 per bulb. Assume a project lifetime of 10 years. The NPV results are tabulated in Table~\ref{tab:LED}, showing the NPV after 10 years is \$5.29.
The same data are shown in Figure~\ref{fig:npv}.

\begin{table}
\centering
\caption{Net present value analysis for changing a light bulb.
\label{tab:LED}}
\begin{tabular}{crrr}
\hline \hline
Year ($t$) & Cash flow ($R$) & Present value (PV) & Net present value (NPV) \\
\hline
	  0	&	-\$7.00	&	-\$7.00	&	-\$7.00	\\
	  1	&	\$2.00	&	\$1.82	&	-\$5.18	\\
	  2	&	\$2.00	&	\$1.65	&	-\$3.53	\\
	  3	&	\$2.00	&	\$1.50	&	-\$2.03	\\
	  4	&	\$2.00	&	\$1.37	&	-\$0.66	\\
	  5	&	\$2.00	&	\$1.24	&	\$0.58	\\
	  6	&	\$2.00	&	\$1.13	&	\$1.71	\\
	  7	&	\$2.00	&	\$1.03	&	\$2.74	\\
	  8	&	\$2.00	&	\$0.93	&	\$3.67	\\
	  9	&	\$2.00	&	\$0.85	&	\$4.52	\\
  10	&	\$2.00	&	\$0.77	&	\$5.29	\\
\hline \hline
\end{tabular}
\end{table}

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{f/npv/out.pdf}
\caption{Net present value. The present values are the bars, the net present value is the curve.\label{fig:npv}}
\end{figure}

\paragraph{Example 2.} The electricity savings in Example 1 were for a light bulb that was operated 365 hr per year. A company might instead ask the question of at how many hours per year should a light bulb be operational in order to justify changing it out for a LED? This would be the point at which the NPV goes to zero.

We can perform an identical analysis, but let the \$2 annual savings (the positive portion of the cash flow) be a variable.
This value can then be changed until the project NPV is zero.\footnote{That is, you can use an optimizer or nonlinear solver to do this. In practice, most people in the business world do this from within a spreadsheet and use the ``goal seek'' feature.}
This results in a required annual savings of \$1.26.
Scaling this (\$1.26/\$2.00) $\times$ 365 we find that a light bulb that is in operation more than 230 hours per year can be justified to be changed to LED based on economic grounds.
The tabulated NPV for this example is in Table~\ref{tab:LED-variable}.

\begin{table}
\centering
\caption{Net present value analysis for changing a light bulb.
\label{tab:LED-variable}}
\begin{tabular}{crrr}
\hline \hline
Year ($t$) & Cash flow ($R$) & Present value (PV) & Net present value (NPV) \\
\hline
0	&	-\$7.74	&	-\$7.74	&	-\$7.74	\\
1	&	\$1.26	&	\$1.15	&	-\$6.59	\\
2	&	\$1.26	&	\$1.04	&	-\$5.55	\\
3	&	\$1.26	&	\$0.95	&	-\$4.61	\\
4	&	\$1.26	&	\$0.86	&	-\$3.74	\\
5	&	\$1.26	&	\$0.78	&	-\$2.96	\\
6	&	\$1.26	&	\$0.71	&	-\$2.25	\\
7	&	\$1.26	&	\$0.65	&	-\$1.60	\\
8	&	\$1.26	&	\$0.59	&	-\$1.02	\\
9	&	\$1.26	&	\$0.53	&	-\$0.48	\\
10	&	\$1.26	&	\$0.49	&	\$0.00	\\
\hline \hline
\end{tabular}
\end{table}

%\end{notyetready}

\endgroup
