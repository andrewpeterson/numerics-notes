\begingroup
\chapter{Design of experiments}

\reading{NIST/SEMATECH e-Handbook of Statistical Methods, Ch.~5~\cite{NISTebook}}

\noindent
We're in an era of `big data'. That's kind of true, and its true in fields like software if you are making an app, or computations, or maybe imaging, like pulling in astronomical data.
But still, good data is often very expensive.
Each data point may cost hundreds or thousands of dollars.
Examples:

\begin{itemize}
\item Agronomy (origin of the field of design of experiments!).
If you are a farmer, it takes you a whole growing season to get each set of data points, and it involves a lot of work.
Farming is extremely competitive, so the money lost in productivity and labor per data point is significant; you only make money if you can grow enough corn on your land to sell it at the market price.
Let's say you want to optimize your yield: that is the bushels of corn per acre of land.
You could divide your land up into plots, then start varying factors on different plots.
\textit{E.g.}, the depth of planting, frequency of watering, time of day of watering, spacing of planting, whether pesticide A is applied, whether pesticide B is applied, and how often, whether nitrogen fertilizer is added (how much?), whether phosphorus fertilizer is added, whether potassium fertilizer is added.

To know anything, you need to start your experiment in April or so, carefully administer each plot's individual treatment, then harvest in August, carefully noting your bushels per acre for each condition.
And on top of that, there's a lot of noise: maybe the soils are a bit different, or some of the land is slightly sloped or in a windier location.
And the experiment probably would come out differently next year when the weather is different.

But the point is, that each data point is expensive, and the measurement is noisy.
So you want to be thoughtful about how you design your experiments so that you can extract the maximum amount of information about it that you can.
The field of design of experiments came out of agronomy where this situation is pretty extreme.

\item Manufacturing.
Let's say you work at a steel manufacturing facility, and you want to optimize the properties of your steel.
You might vary process temperature, pressure, heat ramps, calendaring rates, composition space, etc.
When you run an experiment, you by definition are not running your process to specification, so there's a very good chance that you have to throw out (or recycle) the product of that experiment.
These processes can take long times to approach steady state, so there's a real chance that each data point will take 12 hours or 24 hours to do it right.
This can easily mean that each data point is costing you thousands or even millions of dollars.
Of course, you want to start your testing in a pilot plant to get cheaper data, but at some point you need to try things at full scale.

\item Graduate research.
In Shouheng Sun's lab, they are very good at making nanoparticles with controlled shapes, sizes, and compositions.
They often use a wet synthesis method, where they mix together chemicals in various ratios and have different temperature programs and additives like surfactants that control sizes.
Each time they run experiments, they need to carefully control all these factors, perform the synthesis, purify their products, then do some sort of imaging to see what they made.
And if they are making catalysts they need to do a whole other set of experiments that test how the particles perform as catalysts, and each of those has parameters like temperature, pressure, concentrations of reagents, etc., that must be controlled.
You can imagine that each experiment takes a day at least, and the reagents and things are not cheap.
Even though grad students are cheap, they do want to graduate at some point, so choosing how to explore the unknown variables could make a lot of sense.

\item Cooking.
Perhaps you'd like to ``optimize'' a bread recipe in your kitchen?
While not particularly expensive, this involves a lot of your labor.


\item Beamtime at a national lab.
National laboratory beamtime is very expensive, and they typically ask that you come in with a very well-planned experimental regimen.

\item etc.

\end{itemize}

\noindent
Also, this ``small''  data can often have a large inherent error in each measurement---there is noise, so doing a straight optimization algorithm is not always the best choice.
This is where the field of design of experiments (often abbreviated as DOX or DOE) comes in; it assumes data is expensive, and we should be wise in how we go about collecting it.
DOX is covered in-depth in many good statistics courses, but many students aren't exposed to it before they graduate and enter industry, where the practice is fairly standardized.

Much of the discussion here is based on the NIST reference above; there is also a great old (black and white) video series that lives at the below link:

\url{https://www.youtube.com/watch?v=NoVlRAq0Uxs}

\section{A single independent variable}

We'll first cover if you are trying to uncover the relationship of a single variable.
Here we'll state a few best practices---which should be reasonably obvious, but are worth stating explicitly.
Of course, these are only guidelines, and your particular situation may be different.

\begin{enumerate}
\item Logarithmic spacing is often better than linear spacing.
\textit{E.g.}, if you want to know the response of a reaction conversion to a concentration, you might want to try 0.01, 0.03, 0.1, 0.3, 1.0 M concentrations of the reagent.
This would be much more efficient than 0.01, 0.26, 0.51, 0.75, 1.0.
This is not the case for every variable, but is a good rule-of-thumb.
\item
Randomize your trial order!
If you don't randomize, you are subject to things like drift.
\textit{E.g.}, if you perform the above 5 experiments throughout the day, perhaps the lab is getting slightly warmer throughout the day.
You might notice a small dependence that you attribute to the concentration, but is just the ambient temperature changing.
Or maybe in a manufacturing facility you have the day shift workers performing the first two experiments and the night shift workers the last few; there can be huge differences between shifts that might better explain the results you see.
\item
Perform repetitions!
It's typically a good idea to perform at least a single measurement multiple times to give a clear indication of the inherent variability.
(Ideally, you should perform each important measurement multiple times if they are relatively cheap and relatively important. But if they are very expensive, this isn't always possible, and you have to decide what is more valuable.)
\end{enumerate}

\section{Multi-variate design}

\begin{figure}
\centering
\includegraphics[width=0.45 \textwidth]{f/doe/drawing.pdf}
\includegraphics[width=0.45 \textwidth]{f/doe/drawing2.pdf}
\caption{\label{fig:doe} Two approaches of experimenting with two controlled variables. On the left is the one-at-a-time approach; on the right is the design-of-experiments approach.
}
\end{figure}

When you have many variables, things get complicated quickly.
Imagine the following example shown in Figure~\ref{fig:doe}.
You are trying to optimize the yield of a chemical in a process, and you have the following two control variables: concentration of the reagent and temperature.
The true response is shown in the background---this is, of course, unknown.

Let's say you start at the left hand side, which is today's operating condition.
You try to optimize the concentration while holding the temperature constant, because you know that you should only try to change one variable at a time otherwise you confound effects.
So you do this, eventually finding the ``optimum'' yield with respect to concentration; this is much better than your starting condition.
Then you separately optimize temperature, holding the concentration constant at its ``optimal'' value.
You found the optimal operating condition!
But you are wrong; because the two variables can't be optimized one-at-a-time.
This is the danger of many variables, the one-at-a-time approach doesn't actually work that well.
And this mistake is incredibly common.

If instead you did something like on the right---trying high and low values of each variable, you would have done much better.
\textit{E.g.}, here, you would see that the top right of your square has the best value (and in this particular case is even better than the optimum you found after many more experiments in the other case); and we did this with fewer experiments.
More importantly, it give you the direction to go in your next batch of experiments, since you can clearly see that the surface is pointing upward in a particular direction.

\subsection{Full factorial design}

\reading{NIST/SEMATECH e-Handbook of Statistical Methods~\cite{NISTebook}, \textsection~5.3.3.3}

\noindent
In a full factorial design, we take high and low values of each control variable (plus perhaps the original center point).
This is referred to as $l^k$ design or often $2^k$ design, when just high and low values are used.
For the farming example, we might set our variables at values like:

\vspace{1em}

\begin{tabular}{lll|lll}
\hline
Raw & Low & High & Coded & Low & High \\
\hline
Soil depth & 1 in & 2 in & $X_1$ & -1 & + 1 \\
N amount & 1 lb/acre & 5 lb/acre & $X_2$ & -1 & +1 \\
Watering frequency & 1/week & 1/day & $X_3$ & -1 & +1 \\
\hline
\end{tabular}

\vspace{1em}

In the left half of the table are the raw low and high values that we are choosing for each variable.
Conventionally, these are coded as $X_i$ and the low and high values are referred to as -1 and +1, respectively, such that standardized regression / analysis-of-variance software can be used.


A simple model could be written as

\begin{align*}
Y^\mathrm{pred} = & \alpha_0 \\
& + \alpha_1 X_1 + \alpha_2 X_2 + \alpha_3 X_3 \\		                   
& + \alpha_4 X_1 X_2 + \alpha_5 X_1 X_3 + \alpha_6 X_2 X_3 \\
& + \alpha_7 X_1 X_2 X_3
\end{align*}

\noindent
$\alpha_0$ is the mean. The next row are the ``main effects''.
The following row are two-term and three-term interaction effects.

There are 7 unknown constants, and in our experiment we had $2^3 + 1 = 9$ (where the extra 1 is the central point, which may or may not be used in every experiment) data points, so we can in principle fit them.

In class, we went through Example 5.4.7.1 from the NIST reading, which describes the full analysis of a full factorial design; this will not be reproduced in these notes.

\subsection{Fractional factorial design}

\reading{NIST/SEMATECH e-Handbook of Statistical Methods~\cite{NISTebook}, \textsection~5.3.3.4}

\begin{figure}
\centering
\includegraphics[width=0.45 \textwidth]{f/doe/ffd.pdf}
\caption{\label{fig:doe-ffd}
}
\end{figure}

\noindent
However, if data are expensive, perhaps we can figure out a way to get similar information with fewer experiments?
This is the fractional factorial design.

Imagine that we are doing a $2^5$ factorial design; that is, we have 5 variables, each of which can take on a low and high value.
When we right a model analogous to that shown above, we have coefficients $\alpha_i$ that correspond to:

\begin{itemize}
\item 1 intercept (mean)
\item 5 1-body terms (main effects)
\item 10 2-body terms (interactions)
\item 10 3-body terms (interactions)
\item 4 4-body terms (interactions)
\item 1 5-body terms (interactions)
\end{itemize}

\noindent
for a total of 31 unknown parameters.
In a full factorial design, we would run $2^5 + 1 = 33$ experiments so we would have just enough data to fit these 31 parameters.
However, 3-body effects are rare, and 4- and 5-body effects are extremely rare and difficult to interpret when they do arise.
If instead we assume nothing greater than a 2-body interaction is important, our model only has  16 parameters.
Thus, we could in principle get away with $2^{5-1} + 1 = 17$ individual observations.
This is known as a fractional factorial design, commonly referred to as $l^{k-p}$ or $2^{k-p}$.

But we have to choose the experimental points carefully---here, picking them out of a hat is not good enough.
We need to make sure that the design is balanced and orthogonal:

\begin{itemize}
\item Balanced: ``An experimental design where all cells (i.e. treatment combinations) have the same number of observations.'' (from NIST)
\item Orthogonal: ``An experimental design is orthogonal if the effects of any factor balance out (sum to zero) across the effects of the other factors.'' (from NIST)
\end{itemize}

An example of a balanced and orthogonal design for a $2^3$ design is shown in Figure~\ref{fig:doe-ffd}.
The eight points are the high and low values of the three design variables (optionally, plus the center point) would give the full factorial design.
Choosing just the green points would be a balanced, orthogonal approach.
In practice, these designs are taken from tables or software, which also give a randomized order of experiments.
An example of a fractional factorial design is shown as Example 5.4.7.2 of the NIST reading.


\endgroup
