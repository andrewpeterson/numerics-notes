\begingroup
\chapter{Mixed optimization}

The previous two chapters covered simple optimization of continuous functions (\secref{sxn:opt}) and graph-theory optimization (\secref{sxn:graph}).
Here, we'll briefly go over some last topics on optimization that apply to both topics.


\section{Mixed problems \label{sxn:mixed-problems}}

We have kept our earlier discussion to two discrete topics of optimization: continuous function optimization \textit{vs.} graph theory.
Of course, most challenging optimization problems involve some mixture of continuous and discrete variables.
Solving these problems, in general, is hard; in this section, we'll give some basic concepts but will note that there are fields of specialty that deal with just these sorts of applications.

At its simplest, let's ignore the complexity of graph theory and just assume that we can pose our optimization problems involving continuous variables (\textit{e.g.}, temperature, flow rate, ...) and integers (\textit{e.g.}, in graph theory there is a 1 if the connection is used and a 0 otherwise).
For example, if you are optimizing a process you might have to choose between two pieces of equipment, and the process must be optimized with each choice.
And there may be thousands of pieces of equipment to choose from.

In general, this is mixed-integer non-linear programming.
This is the hardest of optimization problems, but actually is real life.
There typically is not a easy way to solve these, and this is generally the reason that engineers are hired.
There exist solution methods if the problem can be well-posed, but they are computationally challenging and can fall into local minima.
We won't cover that in this course; there are courses on this topic, typically in systems engineering curricula.
Here, we'll just qualitatively cover some basic approaches.

\subsection{Simple problems: combinatorics}

Let's imagine that we have a system that has a mixture of continuous variables (that can take on any floating-point value) and binary variables (that can only be 0 or 1).
For example, if we are optimizing a process, we may have a binary value that is 1 if there is a pump, and 0 if there is not a pump in our process.

If we only had one binary variable, the simplest solution would be to run the optimization twice: once with the variable set at 0, and once with it set at 1.
Then we simply choose the better of the two answers; we use non-linear optimization twice.

This is straightforward if we only have a few binary variables.
For example, if we had two binary variables, then we need to run our optimization under four scenarios: [(0,0), (0,1), (1,0), (1,1)].

However, we can see that this is a combinatoric problem, and the number of scenarios we need to run is $2^n$ if there are $n$ binary variables.
You might be able to pull this off with 10 binary variables,\footnote{$2^{10}=1024$.} but I wouldn't try it with 100.\footnote{$2^{100}=1267650600228229401496703205376$.}

\subsection{Treating as continuous; adding penalties}

Another strategy is to let our integer variables be treated as continuous variables\footnote{Of course, this isn't always possible. For example, if your binary variable indicates if there is or is not a pump, we really don't have a way to write a model with half a pump. But you'd be surprised what people can pull off: I have seen people define fake chemical elements with atomic numbers between those of the real elements on the periodic table!} during the optimization.
\textit{I.e.}, we might allow a binary variable to take on any continuous value in the closed interval [0,1].
We check our answer at the end of the routine, and if the optimum solution has chosen exactly 0 or 1, we are in good shape.
If not, we impose a penalty in our loss function for non-integer values using the techniques of \secref{sxn:math-constrained}; if the constraint is large enough it will force our solution to choose an integer value, at least to some arbitrary precision.
(This is similar to what you did in your homework where you had a penalty to force the points to lie on a parabola.)
Of course, how and when you implement this penalty will certainly make a difference in how you get to the ultimate solution.


\subsection{Branch and bound}

The branch-and-bound technique is systematic approach for dealing with integer variables in an otherwise continuous landscape; it is particularly useful if the integer variables can take on a wide range of values (\textit{i.e.}, they are not just binary), and if the problem can be expressed as a purely linear problem, so that you will find the global minimum easily (when the variables are continuous).
In this case, it will find the global optimum solution in a finite amount of time.

You can readily find examples of the implementation of this method online, which illustrate it nicely.
Briefly, the discrete variables are ``relaxed'' into continuous variables and the problem is optimized.
This provides an initial lower bound\footnote{Assuming we are performing a minimization; that is, a best-case scenario.} for the optimal solution.
For discrete variables whose optimal solution comes out to be a non-integer, the solution tree is branched.
For example, if the variable $x_1$ is only allowed to take on integer values and upon optimization, $x_1= 7.4$, then we ``branch'' the solution into two problems: one with $x_1 \le 7$ and one with $x_1 \ge 8$.
We then perform constrained optimization on each of these branches; and we now have bounds on each of these branches.
Ultimately, we are able to compare and the best-case scenario solutions to each of these branches, and eliminate solution branches until we find the optimal case.

\subsection{Mixed-integer non-linear programming}

If we are lucky enough that our optimization function can be written in a linear form, even if we have a mixture of linear and continuous variables, then we have ready solution approaches available, such as branch and bound.
However, for the general case where we have a mixture of both types of variables and our functional form is the general non-linear case---most cases in engineering design!---then we have mixed-integer non-linear programming, which is generally considered the hardest class of optimization problems.
There are typically not easy ``off-the-shelf'' methods to deal with these, and the optimal method depends on the physical problem being solved.
There are many specialized research areas that examine problems in various disciplines.
(Perhaps we wouldn't have engineers if there was a simple, general solution to this problem.)

\section{Global optimization}

A second key point raised when we first introduced optimization in \secref{sxn:opt-key-points} is: finding a local optimum is usually rather straightforward, but finding the global optimum solution is in general one of the hardest problems in engineering.
In general, there are not easy (that is, computationally feasible) solutions to this problem, even for the case when all of your free variables are continuous.

The approach you take will depend in large part on what your problem is.
For example, I showed a paper that we published\cite{Peterson2014} that uses a technique called minima hopping.
In this approach, atomic structures are launched out of local minima (by numerically integrating their equations of motion), and relaxed from new, random configurations; this also includes self-adjust parameters that adjust the momentum at which they are launched as well as the probability of accepting newfound minima.
We also included custom constraints that act to preserve molecular identity throughout this process.

Perhaps the most famous global optimization approach is simulated annealing.
This is based on the physical process of annealing---where a material (\textit{e.g.}, steel) is very slowly cooled from a high temperature.
In materials, this helps the structure form a ``perfect'' crystal, as it is slowly lowered in energy.
In simulated annealing, we think of the loss function as the energy and we ``cool'' the system.
That is, we initially assume it has some temperature so that it can sample states, then we slowly lower that temperature so that it just samples the lowest-energy state.

Practically, we assume we are at some state $s$, and we propose a move to $s'$.
We accept the move with a probability like

\[ P ~\sim \exp \left\{ \frac{- (E(s') - E(s)}{T} \right\} \]

\noindent
For those of you with a statistical mechanics background, this should be very familiar.
This is the probability given to us by Boltzmann,\footnote{In the true Boltzmann factor, the denominator is $k_\mathrm{B} T$, where $k_\mathrm{B}$ is the Boltzmann constant.}  which is the basis of much behavior predicted by statistical mechanics.


\endgroup
