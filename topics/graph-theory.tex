\begingroup
% Augmented matrix.
\newenvironment{amatrix}[1]{ \left[\begin{array}{@{}*{#1}{r}|r@{}} }{ \end{array}\right] }
% Partial differential.
\newcommand{\pd}[3]{\left(\frac{\partial #1}{\partial #2}\right)_{#3}}
% Norm.
\newcommand{\norm}[1]{\left\lVert #1 \right\rVert}
% Minimize with underscore.
\newcommand{\minn}[1]{\underset{#1}{\mathrm{minimize}}}
\chapter{Graph theory\label{sxn:graph}}

\reading{K23}

\noindent
Now we'll turn our focus to a different subject: graph theory.
Graph theory is inherently the study of discrete objects, and how they are connected.
The problems of graph theory are often with how to structure something (or how to interpret a structure).
Some examples:

\begin{itemize}
\item GPS street directions: There are billions of ways to drive from here to St. Louis; how to pick something good?
\item How to schedule logistics?
\item How to lay out a manufacturing plant?
\item What are the possible moves I can make in chess? (and the opponents' moves? and my follow-ups?) This used to be how computerized chess worked; now they seem to be switching over to two neural networks playing each other.
\item How to solve a Rubik's cube?
\item How to optimally lay out a circuit?
\item How to describe connectivity in a molecule?
\item How to route traffic over the internet?
\item How a computer can know when a piece of memory is no longer in use?
\item How are bonds structured in a molecule or protein?
\item How many degrees of separation are there between me and Nils Bohr?
\item How do websites link to one another?
\item How can phone calls be routed through switchboards?
\end{itemize}

The solution of these problems is inherently computational in nature; we generally can't write down neat mathematical forms for solutions to problems.
Let's take an example; say you are playing the game tic-tac-toe; a game with just nine board places, and you want to determine the next outcome.
See Figure~\ref{fig:tictactoe}; if we start from a certain configuration, we can draw the next configurations as shown in the figure.
From each of these, we could branch out to the next possible configurations, that represent the next turn by the opposing player.
In the end, we would construct a \emph{graph} that describes all possible moves remaining in this game.

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/graph-theory/tictactoe.pdf}
\caption{\label{fig:tictactoe}
Tic tac toe, as graph theory.
}
\end{figure}




\section{Formalism}

See the graphs drawn in Figure~\ref{fig:graph-theory}, which we'll use to define the terminology we'll use to describe a graph.

\begin{figure}
\centering
\includegraphics[width=0.65 \textwidth]{f/graph-theory/drawing.pdf}
\caption{\label{fig:graph-theory} Example of graphs. The left example is an undirected graph, while the right is a directed graph.
}
\end{figure}

\begin{itemize}
\item \emph{Vertex.} The points A, B, C, D, E are called verteces; these are often also referred to as \emph{nodes}.
(In our tic-tac-toe example, each configuration of the game board is a vertex; in a molecule, each vertex corresponds to an atom (nucleus).)
\item \emph{Edge.} The connections between verteces are called edges.
That is, vertex D has one edge connecting it to vertex A.
(In the tic-tac-toe example, an edge corresponds to a ``move''; in a molecule, an edge corresponds to a bond.)
\end{itemize}

\noindent
In Figure~\ref{fig:graph-theory}, we show two basic types of graphs.
\begin{itemize}
\item
\emph{Undirected graph.} The direction of the edges doesn't matter. A molecule would be an example of an undirected graph.
\item
\emph{Directed graph.} Each edge has a direction; that is, the edges represent some transition or flow between verteces.
In our tic-tac-toe example, the edges are moves in a game, and since we can't ``un-do'' a move, only forward moves are allowed.
\end{itemize}

In compact nomenclature, we can represent the verteces of a graph as a set:

\[ V = \{ A, B, C, D, E \} \]

\noindent
Correspondingly, the edges are the set of connections in the verteces:

\[ E = \{ (A, B), (A, D), (A, E), (B, C), (C, E) \} \]

\noindent
If the graph is undirected, then the ordering within each pair is arbitrary (e.g., (A,B) and (B,A) represent the same thing), while if it is directed, then the ordering matters (e.g., (A,B), means there is an arrow in the direction A$\rightarrow$B).
The complete graph is then just a collection of verteces and edges, which we can denote compactly as:

\[ G = (V, E) \]

\paragraph{Adjacency.}
The book---unwisely, in my opinion---states that you should use an adjacency matrix to describe a graph on a computer.
For example, the adjacency corresponding to the two graphs of Figure~\ref{fig:graph-theory} are, respectively,

\[ \m{A} = 
\brm{0 & 1 & 0 & 1 & 1 \\
    1 & 0 & 1 & 0 & 0 \\
	 0 & 1 & 0 & 0 & 1 \\
	 1 & 0 & 0 & 0 & 0 \\
	 1 & 0 & 1 & 0 & 0 },
\hspace{1em}
\m{A} = 
\brm{0 & 1 & 0 & 0 & 1 \\
	0 & 0 & 0 & 0 & 0 \\
	0 & 1 & 0 & 0 & 0 \\
	1 & 0 & 0 & 0 & 0 \\
0 & 0 & 1 & 0 & 0 } \]

\noindent
Note that the undirected graph has a symmetric adjacency matrix, while the directed graph does not.

However, in my opinion the use of a matrix for this purpose is a terrible idea, for the following reasons:

\begin{itemize}

\item
Generally, most verteces are connected to very few other verteces.
(\textit{E.g.}, in a matrix describing how the world-wide web is connected, most individual websites do not link to one another.)
Thus, this will be a huge matrix containing almost all zeros, with occasional 1's.
This will take up a huge amount of memory, and computations on it will be expensive.

(Another way of saying this is that it's a sparse matrix.
One solution would be to choose a sparse matrix type, as described in section \secref{sxn:sparse}.)

\item
To construct such a matrix, we need to pre-compute all entries in this matrix.
That is, we need to find all the connections.
This can be extraordinarily complicated.

Think of an example problem: we are going to mine openstreetmaps and use it to make our own driving directions app to compete with google maps, but we'll focus on walking directions.
If we use an adjacency matrix, before we compute walking directions anywhere, we need to figure out how to identify ever possible vertex and node and how they are connected in the world.
This would take a long time to assemble, and the resulting matrix might be too big to put on a smartphone.
But if instead, let's say we start at a vertex, it's easy to just check and see what roads come out of it to know what its connected to: that is, generate the connections on the fly.

As a second algorithm, consider building an algorithm to play chess.
If we have a certain configuration on the board, I can calculate the next configuration of pieces---that is, the possible moves of my opponent---and my possible next moves, for example.
However, why should I first have to worry about every other configuration of the board that I'm not likely to encounter?
It's easier to compute on-the-fly.

In your homework, you compute distances on the Brown Engineering website.
That is, if you start at a particular website, how many clicks does it take to get to another specified webpage?
You will in this case generate it on-the-fly.
\end{itemize}

It's better to consider adjacency as a function.
\textit{E.g.},

\[ \mathrm{Adj}(A) = {B, D} \]

\noindent
Practically, in python, this could either be a dictionary or a function/class.

\section{Exploring graphs / shortest-distance problem \label{sxn:exploring-graphs}}

The simplest, and perhaps most common problems in graph theory are the closely related problems of finding connections with a graph and finding the shortest distance between two verteces A and B.
Here, we'll assume that all edges (connections between verteces) are of identical length; we'll relax this constraint in subsequent algorithms.


\subsection{Breadth-first algorithm: Moore's algorithm.}
\reading{K23.2}

\noindent
This is a brute-force algorithm, which it seems was first invented by Konrad Zuse.
Zuse did other famous stuff like invent the programmable (Turing-complete) computer and the first high-level programming language.
In his PhD thesis of 1945, he invented the algorithm we are about to discuss; however, he forgot to pay his enrollment fee and his PhD was rejected; thus, he didn't publish his algorithm in the open literature until decades later.

Meanwhile, Edward Moore---a Brown alum---invented the same thing about a decade later, and published it in the open literature.
As the first to publish, our book names the algorithm after him.
However, the algorithm is simple it has probably been invented independently thousands of times.

\begin{figure}
\centering
\includegraphics[width=0.34 \textwidth]{f/breadth-first/drawing-numbered.pdf}
\hspace{0.1 \textwidth}
\includegraphics[width=0.34 \textwidth]{f/breadth-first/drawing-letters.pdf}
\caption{\label{fig:breadth-first}
Illustration of Moore's (``breadth-first'') algorithm.
}
\end{figure}

Here, we'll describe this while looking at an example; see Figure~\ref{fig:breadth-first}, focusing on the left-most drawing.
Let's assume we start at the vertex labeled ``start'' and want to know the shortest distance to the vertex labeled ``finish'', again assuming all connections have the same length.
Here, we have a drawing of our graph where we can see all the connections, but we'll try to keep in mind that we want this algorithm to work where we don't known \textit{a priori} what the connections are between any two nodes.

We start by labeling our first node with the number 0.
We find its neighbors (that is we take the ``adjacency'' function of this vertex) and label each of them with the number 1.
Then, from each node 1 we find their new neighbors, and label each of them as 2.
And we continue until we find the node which we marked ``finish'' in our figure.

\paragraph{Exploration vs.\ shortest distance.}
Note that this algorithm accomplishes two different things.
First, it finds our shortest distance, as advertised above.
But second, it also explores the graph and finds the connections; that is, if we did not know what the graph looked like we could build an adjacency table in this manner.
For example, let's consider that we want to crawl through and find all the websites, so that we can build a search engine.
We can start with an origin website and use all the links within this as its adjacency.
The webpages these links point to are labeled 1; we do the same from each of them, labeling the new websites as 2.
Thus we simultaneously explore the web (finding all connections) and build out a distance table from our original website.
(We could then use the adjacency table or matrix with the PageRank algorithm of \secref{sxn:pagerank}, for example, to rank search results.)
In this incarnation, finding the shortest distance and finding the graph's connectivity are the same problem (or at least share a solution method).

\paragraph{How to do this on a computer?}
This seems easy if we can draw connections on a chalkboard.
But how can we automate this for a large system where we don't have a drawing available?

Here, we'll use a combination of a queue and a step table; of course, you should modify this approach to your particular problem.
In this approach, let's label the nodes as in the right-side of Figure~\ref{fig:breadth-first}, and in our problem we'd like to find the shortest distance from node I to node C.

We can assemble the queue and step table as follows; the results are shown in Table~\ref{tab:moore-table}.
We start with an empty queue and add (I,0) to it, which is our origin node; the 0 means we can reach it in zero steps from our origin mode.
Then we remove it from the queue and add it to our step table.
When we process it, we use our adjacency function to find its neighbors, which are in this case G and K.
We add each of these to the bottom of the queue, along with 1 indicating we can reach them from the origin in 1 step.
We take the top element off of the queue (which now is ``(G,1)'') and add it to the step table, generating its new neighbor J.
(J,2) is added to the bottom of the queue.
We continue in this manner until we discover node C, which here we can see we can reach in 4 steps.


\begingroup
\renewcommand{\r}[1]{\color{red} ``\ensuremath{#1}''}
\newcommand{\rf}{\color{red} ``\ensuremath{\infty}''}

\begin{table}
\centering
\caption{Moore's algorithm as we might do it on a computer, with a queue and a list of found objects. Note that items are crossed off the queue from the bottom down.\label{tab:moore-table}}
\begin{tabular}{l}
\hline \hline
Queue \\
\hline
(I, 0) \\
(G,1) \\
(K, 1) \\
(J, 2) \\
(H, 2) \\
(A, 3) \\
(E, 3) \\
(F, 3) \\
(D, 3) \\
(B, 4) \\
(C, 4) \\
\hline \hline
\end{tabular}
\hspace{4em}
\begin{tabular}{lll}
\hline \hline
Node & Distance & New neighbors \\
\hline
I & 0 & \{ G, K \} \\
G & 1 & \{ J \} \\
K & 1 & \{ H \} \\
J & 2 & \{ A, E, F \} \\
H & 2 & \{ D \} \\
A & 3 & \{ B \} \\
E & 3 & \{ \} \\
F & 3 & \{ \} \\
D & 3 & \{ C \} \\
B & 4 & \{ \} \\
C & 4 & \{ \} \\
\hline \hline
\end{tabular}
\end{table}
\endgroup

The key data structure here is the queue, which is just a list that we can append items to and take the top item off of.

\subsection{Dijkstra's algorithm}
\reading{K23.3}

\noindent
An alternative approach is Dijkstra's algorithm.
This approach lends itself naturally to situations where the edges have different lengths, as in Figure~\ref{fig:dijkstra}.
This is kind of a neat approach that relies on temporary labels which get converted one-by-one to permanent labels; it gives distances from an origin to everywhere, and the algorithm can be stopped at any point to give the maximum distance from the start to particular node.

\begin{figure}
\centering
\includegraphics[width=0.4 \textwidth]{f/dijkstra/drawing.pdf}
\caption{Graph for Dijkstra's algorithm.\label{fig:dijkstra}
}
\end{figure}

{
\renewcommand{\r}[1]{\color{red} ``\ensuremath{#1}''}
\newcommand{\rf}{\color{red} ``\ensuremath{\infty}''}

\begin{table}
\centering
\caption{Dijkstra's algorithm.\label{tab:dijkstra}}
\begin{tabular}{lccccccc}
\hline \hline
Step & A & B & C & D & E & F & G \\
\hline
0 & 0 & \r{2} & \r{1} & \r{3} & \rf & \rf &  \rf \\
1 & 0 & \r{2} & 1 & \r{3} & \rf & \r{5} &  \rf \\
2 & 0 & 2 & 1 & \r{3} & \r{5} & \r{4} &  \rf \\
3 & 0 & 2 & 1 & 3 & \r{5} & \r{4} &  \r{8} \\
4 & 0 & 2 & 1 & 3 & \r{5} & 4 &  \r{7} \\
5 & 0 & 2 & 1 & 3 & 5 & 4 & \r{7} \\
6 & 0 & 2 & 1 & 3 & 5 & 4 & 7 \\
\hline \hline
\end{tabular}
\end{table}
}

We'll first work through the mechanics of the algorithm, then we'll come back to describe its logic.
Let's work through the example in Figure~\ref{fig:dijkstra} and Table~\ref{tab:dijkstra}, where we'll take A as the origin node.
Algorithm:

\begin{enumerate}
\item
Start with a permanent label of 0 on the starting position.
In the table, we'll denote the permanent labels with black text.
Put temporary labels of $l_{\mathrm{A}i}$ for all other $i$ vertices, where $l_{\mathrm{A}i}$ is the distance from node A.
We'll denote temporary labels in red text, and inside quotation marks.

\item
Flip the lowest-valued temporary label into a permanent label.

\item
For any neighbors of this ``flipped'' vertex, check the distance to the origin via the flipped vertex, and update the temporary label if the new proposed distance is lower than the old one.

\item Go to \#2.

\end{enumerate}

Dijkstra's algorithm is used in driving directions algorithm; \textit{e.g.}, we looked at the open-source code graphhopper, and found that under the hood it had a routine called dijkstra.

\subsubsection{The logic of Dijkstra's algorithm}

Let's look at this algorithm from a logical point of view, and why we can switch from temporary to permanent labels.
Let's start with just the stub of the graph shown in Figure~\ref{fig:dijkstra-initial-only}.

\begin{figure}
\centering
\includegraphics[width=0.18 \textwidth]{f/dijkstra/initial-only.pdf}
\caption{\label{fig:dijkstra-initial-only}
The first choices in Dijkstra's algorithm.
}
\end{figure}

I like to think of the verteces as cities, and the edges as bus fares between cities.
Then we ask, what is the cheapest way to get from one city to another?
Here, we'll use the bus-fare analogy as we describe the logic.
We start at city A, from where we can get to city B for \$2, C for \$1, and D for \$3; these are all the buses that leave from city A.
Now, we'll start to make a table of minimum bus fares from A, and we'll put in placeholder values for the minimum fares to each city, as the first row in Table~\ref{tab:dijkstra-initial-only}.
That is, we know we shouldn't pay \emph{more} than \$3 to get from A to D.

\begingroup
\renewcommand{\r}[1]{\color{red} ``\ensuremath{#1}''}
\newcommand{\rf}{\color{red} ``\ensuremath{\infty}''}
\begin{table}
\centering
\caption{Caption\label{tab:dijkstra-initial-only}}
\begin{tabular}{lccccccc}
\hline \hline
Step & A & B & C & D \\
\hline
0 & 0 & \r{2} & \r{1} & \r{3} \\
0 & 0 & \r{2} & 1 & \r{3} \\
\hline \hline
\end{tabular}
\end{table}
\endgroup

But could we pay less?
It's possible---for example, when we examine the bus fares from city C, perhaps we'll find there is a link from C to D for \$1, which would give us a route that costs (in total) less than the \$3 route we have found to date.
This same logic applies to traveling to city B---we might find a \$0.50 fare from C to B, for example.
However, for fares to city C, we can confidently say we'll never find a route that costs less than \$1 by choosing a different route (as we would have already spent more than that to get to B or D).
Therefore, we're justified in converting the price to C into a permanent label; we can confidently say we will not get there for less than \$1, so this is the shortest distance (bus fare) from A to C.
Then, we just continue this logic, and start exploring where we can go from C, adding in more temporary labels that are opened up from our knowledge of the cheapest (total) fare from A to C.

\section{Shortest spanning trees}
\reading{K23.4--K23.5}

\noindent
The last prototypical graph theory problem we'll examine is that of shortest spanning trees.
This problem can be stated as: given a set of verteces and set of possible paths, what is the least-expensive way to all the verteces?
\textit{E.g.}, what is the cheapest way to get houses hooked to the sewer line?
How do I route traffic over the internet?
How are taxonomic trees built?
The name for this problem is ``shortest spanning trees'', and here we'll define the terms in the name before describing how they can be solved.

\begin{figure}
\centering
\includegraphics[width=0.45 \textwidth]{f/sst-cycles/drawing.pdf}
\caption{\label{fig:sst-cycles} Some trees. (The heavy red lines indicate the chosen connections in the tree; the black lines are possible choices.) If the last connections (circled) are made then each tree will become a cycle.
}
\end{figure}

\paragraph{Definition: tree.}
Some graphs are shown in Figure~\ref{fig:sst-cycles}.
The black lines are possible connections, while the red lines are the ``chosen connections''.
A tree is a connected set of edges that have no cycles.
We can see that if we connect the points chosen in green, that we will no longer have a cycle.

Trees are useful for this sort of problem.
In the graph shown on the left of Figure~\ref{fig:sst-cycles}, it would be ``wasteful'' to add the circled edge, because we can already get from the top vertex to the bottom vertex; the cycle would be a redundant path.

\paragraph{Shortest spanning tree.}
With this definition of a tree, the shortest spanning tree is the shortest possible tree (in terms of total edge length) that can get us from any point to any other point.
That is, the smallest tree that fully connects the graph.

\paragraph{Solution strategies.}
In normal years---when there's no pandemic and every one is in class at the same time---we break into groups and each group spends about 20 minutes trying to come up with strategies to solve this problem.
In prior years, all of the strategies presented were good basic ideas, and I was convinced that given a more reasonable amount of time (hours or days, not minutes), that we could re-invent good solution methods.
Here, we'll briefly present two solution strategies that are also contained in the textbook.

\subsection{Kruskal's greedy algorithm}
\reading{K23.4}

\noindent
This approach is very simple (until we try to actually use it).
It goes:

\begin{enumerate}
\item
Order all the edges from shortest length to longest length.
(Note that the ``length'' is often a cost, like the cost of connecting two points with a sewer line.)

\item Starting from the cheapest (shortest length):
\begin{enumerate}
\item Reject if it forms a cycle with the other edges chosen.
\item Otherwise accept.
\end{enumerate}

\item Stop when we have selected $(N_\mathrm{verteces}-1)$ edges.
\end{enumerate}

\paragraph{How do we know when we have a cycle?}
To use this algorithm, we need to be able to identify when we have formed a cycle.
This is pretty obvious in Figure~\ref{fig:sst-cycles} on the left, when we only had three verteces involved.
However, for the case on the right it is harder to see, and for much larger problems we certainly can't rely on a visual solution.


A scheme exists to track this, and it is the double-label scheme to attach labels to the verteces.
We add to the algorithm:
\begin{itemize}
\item Every time we touch a vertex, add the label: $(r_i, p_i)$:, where the first is the root of the tree to which it belongs and the second is its predecessor.\footnote{I actually think it can make more sense to instead list the children, but we'll just keep the textbook approach here.}

\item If an edge contains two verteces with the same root, reject it.
\end{itemize}

\noindent
An example of this, that we went through in class is shown in Figure~\ref{fig:sst-cycles-double-label}.

\begin{figure}
\centering
\includegraphics[width=0.95 \textwidth]{f/sst-cycles/double-label.pdf}
\caption{\label{fig:sst-cycles-double-label}
	The double-label approach to Kruskal's greedy algorithm.
}
\end{figure}


\subsection{Prim's algorithm}
\reading{K23.4}

\noindent
Another method is called Prim's algorithm; this is similar in spirit to Dijkstra's algorithm in which we use a table to keep track of the best connections we have found to date, and update them row-by-row.
Much more elaborate descriptions are available in the text; here we give a brief summary and an example in Table~\ref{tab:prim} (using the same graph as the previous algorithm).
\begin{enumerate}
\item
We choose an initial node, and make a table that contains the shortest distances we have found so far to every other node.
\item
We choose the node that we can reach in the shortest distance from our current tree, and add it to our tree.
We examine the new neighbors of this node, and update the table to contain the distance from the tree to these neighbors.
\item
Go to \#2.
\end{enumerate}


{
\newcommand{\f}{\ensuremath{\infty}}
\begin{table}
\centering
\caption{Example of Prim's method, starting from node 1.\label{tab:prim}}
\begin{tabular}{lccccccr}
\hline \hline
Step & 1 & 2 & 3 & 4 & 5 & 6 & Add vertex  \\
\hline
0 & - & \boxed{2} & 4 & \f & \f & \f & (1,2) \\
1 & - & - & \boxed{4} & 11 & \f & \f & (1,4) \\
2 & - & - & - & 8 & \f & \boxed{1} & (3,6)  \\
3 & - & - & - & \boxed{8} & 9 & - & (3,4) \\
4 & - & - & - & - & \boxed{6} & - & (4,5) \\
5 & - & - & - & - & - & - \\
\hline \hline
\end{tabular}
\end{table}
}

\section{Other types of graph theory problems}

Most students have not dealt with graph theory before, and in this class we can only go over some basic introductory problems.
Full classes are available on graph theory through mathematics and computer science departments which provide much more in-depth theory, along with rich theoretical descriptions.
Here, we have only focused on the problem formulation as well as the basic logic employed.
We have basically looked at three types of problems:

\begin{enumerate}
\item Exploration -- finding all the connections (edges) with Moore's algorithm. If the edges are all of the same length, this also gives you the shortest distance from node A to any other node.
\item Shortest distance -- if the graph is already known, the shortest distance from node A to any other point can be found with Dijkstra's algorithm. Note that this actually gives you a \emph{tree} from the start point to all other points, where this is a shortest-distance tree from node A.
\item Shortest spanning trees -- what is the minimum size graph I can create that connects all my nodes? E.g., getting all the houses in a town on the internet, but restricting the cables that I run to only run on existing streets. We looked at two different algorithms for this problem, one that focused on the verteces and one that focused on the edges.
\end{enumerate}

We abstractly discussed a couple of other applications of graph theory; we only focus on the problem statement, so you can think about other applications of graph theory.
Developing solution methods for all of these sorts of problems is well beyond the scope of the course.


\subsubsection{Map coloring (graph coloring)}

Another famous problem of graph theory is motivated by drawing maps: if I want to color each state / country on my map with a color distinct from its neighbors, how many colors do I need and how do I arrange them?
For a two-dimensional map, this has been proven to be capable of being solved with a palette of four colors.

\begin{figure}
\centering
\includegraphics[width=0.48 \textwidth]{f/map-coloring/Labelled_US_map.pdf} \\
\includegraphics[width=0.48 \textwidth]{f/map-coloring/ContiguousUSAGraph.png}
\includegraphics[width=0.48 \textwidth]{f/map-coloring/ContiguousUSAGraphColorings_1000.png}
\caption{Map coloring of the United States.
	Map from \url{https://upload.wikimedia.org/wikipedia/commons/6/6b/Labelled_US_map.svg}.
	Graphs from \url{http://mathworld.wolfram.com/ContiguousUSAGraph.html}.
\label{fig:map-coloring}
}
\end{figure}

An illustration of this for the states of the United States is shown in Figure~\ref{fig:map-coloring}.
This type of problem is more challenging for more dimensions, and this type of algorithm is useful in other types of applications such as scheduling.


\endgroup
