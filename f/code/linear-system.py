import numpy as np


# Make some random data.
n = 40
A = np.random.rand(n, n)
b = np.random.rand(n)

# Solve it with the "best" solver.
x = np.linalg.solve(A, b)
print(x)

# Solve it by inverting A.
Ainv = np.linalg.inv(A)
x2 = np.matmul(Ainv, b)
print(x2)
