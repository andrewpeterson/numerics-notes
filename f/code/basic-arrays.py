import numpy as np


A = np.array([[3., 4.], [2., 1]])
b = np.array([8., 2.])

print('A:')
print(A)
print(A.shape)
print('b:')
print(b)
print(b.shape)

# Example of matrix muliplication.
# (Don't use "*")!
c = np.matmul(A, b)
print('c:')
print(c)
print(c.shape)
