import numpy as np


n = 40
A = np.random.rand(n, n)
b1 = np.random.rand(n)
b2 = np.random.rand(n)

# Solve them separately.
x1 = np.linalg.solve(A, b1)
x2 = np.linalg.solve(A, b2)

# Solve them together, as a family.
B = np.column_stack([b1, b2])
X = np.linalg.solve(A, B)
