import numpy as np
from matplotlib import pyplot


fig, axes = pyplot.subplots(figsize=(8., 4.), ncols=2)
fig.subplots_adjust(left=0.08, right=0.99)

ax = axes[0]
ax.plot([0., 1.], [0., 0.], '-', color='0.5', lw=3)
ax.plot([0., 0.], [0., 1.], '-', color='0.5', lw=3)
ax.plot([0., 1.5], [0., 0.], 'r-')
ax.plot([1.5, 1.5], [0., 1.6], 'r-')
ax.plot(1.5, 1.6, 'r.')
ax.text(1.5/2, -0.2, '1.5', color='r', ha='center')
ax.text(1.6, 1.6/2, '1.6', color='r', va='center')
ax.set_title('Cartesian basis')

ax = axes[1]
ax.plot([0, 1], [0, 2], '-', color='0.5', lw=3)
ax.plot([0, 2], [0, -1], '-', color='0.5', lw=3)
c1 = .94
c2 = .28
ax.plot([0, 1 * c1], [0, 2 * c1], '-', color='r')
ax.plot([1 * c1, 1 * c1 + 2 * c2 ], [2 * c1, 2 * c1 - 1 * c2], '-', color='r')
ax.plot(1 * c1 + 2 * c2, 2 * c1 - 1 * c2, 'r.')
ax.text(0., 1., str(c1), color='r')
ax.text(1.1, 2., str(c2), color='r')
ax.set_title('Transformed basis')

for ax in axes:
    ax.set_xlim([-1.5, 2.5])
    ax.set_ylim([-1.5, 2.5])
fig.savefig('out2.pdf')
