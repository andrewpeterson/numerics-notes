import numpy as np
from matplotlib import pyplot
from scipy.integrate import solve_ivp


def get_yprime(t, y):
    return - y + alpha * (1. + np.sin(beta * t))


fig, axes = pyplot.subplots(figsize=(9., 4.), ncols=2)
fig.subplots_adjust(bottom=0.13, left=0.08, right=0.99, top=0.93)

max_tau = 10.
taus = np.linspace(0., max_tau, num=500)

ax = axes[0]
beta = 1.
for alpha in [0.1, 1., 2.]:
    ans = solve_ivp(fun=get_yprime, t_span=(0., max_tau), y0=[1.], t_eval=taus)
    ax.plot(ans.t, ans.y[0], label=r'$\alpha={}$'.format(str(alpha)))
ax.set_title(r'$\beta={}$'.format(str(beta)))

ax = axes[1]
alpha = 1.
for beta in [0.1, 1., 10.]:
    ans = solve_ivp(fun=get_yprime, t_span=(0., max_tau), y0=[1.], t_eval=taus)
    ax.plot(ans.t, ans.y[0], label=r'$\beta={}$'.format(str(beta)))
ax.set_title(r'$\alpha={}$'.format(str(alpha)))

for ax in axes:
    ax.set_xlabel(r'$\tau$')
    ax.set_ylabel(r'$\theta$')
    ax.legend(loc='best')

fig.savefig('source.pdf')
