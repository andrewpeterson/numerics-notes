import numpy as np
from matplotlib import pyplot

fig, ax = pyplot.subplots(figsize=(4., 4.))
fig.subplots_adjust(bottom=0.13, left=0.13, right=0.99, top=0.99)
taus = np.linspace(0., 5.)
thetas = np.exp(-taus)
ax.plot(taus, thetas)
ax.set_xlabel(r'$\tau$')
ax.set_ylabel(r'$\theta$')
fig.savefig('no-source.pdf')
