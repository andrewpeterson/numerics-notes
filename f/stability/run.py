import numpy as np
from scipy.optimize import fsolve
from scipy.integrate import odeint
from matplotlib import pyplot


def get_jacobian(yss):
    """Returns the Jacobian at a given steady-state solution."""
    y1, y2 = yss
    J = np.array([[2. * y1 + 1, -1.],
                  [-1., -6. * y2 + 2.]])
    return J


def get_dydt(y, t=None):
    """Returns the original differential equations."""
    y1, y2 = y
    dy1dt = y1**2 + y1 - y2 - 1.
    dy2dt = - y1 - 3. * y2**2 + 2. * y2 + 2.
    return [dy1dt, dy2dt]


# First we'll examine the root around (1, 1).
yss = fsolve(func=get_dydt, x0=(1., 1.))
J = get_jacobian(yss)
eigs = np.linalg.eig(J)
print('The eigenvalues for yss={} are {}.'
      .format(str(yss), str(eigs[0])))

# Let's try a few different initial guesses.
fig, (ax, ax2) = pyplot.subplots(figsize=(8., 4.), ncols=2)
fig.subplots_adjust(left=0.10, right=0.99, bottom=0.10, top=0.98, wspace=0.25)
times = np.linspace(0., 1., num=50)
epsilons = []
for epsilon1 in [-0.1, 0.1]:
    for epsilon2 in [-0.1, 0.1]:
        epsilons.append((epsilon1, epsilon2))
for epsilon1 in [-0.05, 0.05]:
    for epsilon2 in [-0.05, 0.05]:
        epsilons.append((epsilon1, epsilon2))
for epsilon1 in [-0.01, 0.01]:
    for epsilon2 in [-0.01, 0.01]:
        epsilons.append((epsilon1, epsilon2))
for epsilon in epsilons:
        ans = odeint(func=get_dydt, y0=yss + epsilon, t=times)
        ax.plot(times, ans[:,0], color='C0', lw=0.5)
        ax.plot(times, ans[:,1], color='C1', lw=0.5)
        ax2.plot(ans[:,0], ans[:,1], color='C3')
ax.set_xlabel('$t$')
ax.set_ylabel('$y_1$, $y_2$')
ax.set_ylim(-0., 2.)
ax.set_xlim(0., 1.)
ax2.set_xlim(0., 2.)
ax2.set_ylim(0.75, 1.25)
ax2.set_xlabel('$y_1$')
ax2.set_ylabel('$y_2$')
fig.savefig('out.pdf')
