import numpy as np
from matplotlib import pyplot
from scipy.integrate import solve_ivp

def get_dydt(t, y):
    """The SIR differential equations."""
    S, I = y
    dSdt = - R0 * I * S
    dIdt = + R0 * I * S - I
    return [dSdt, dIdt]


R0 = 2.5  # basic reproductive number
I_t0 = 0.001
S_t0 = 1. - I_t0
ans = solve_ivp(get_dydt, t_span=[0., 10.], y0=[S_t0, I_t0],
                dense_output=True)

# Plot it!
fig, ax = pyplot.subplots()
times = np.linspace(0., 10.)
ys = ans.sol(times)  # This is an easy way to extract the answer.
Ss, Is = ys
Rs = 1. - (Ss + Is)
ax.plot(times, Ss, label='S')
ax.plot(times, Is, label='I')
ax.plot(times, Rs, label='R')
ax.legend()
ax.set_xlabel(r'$\tau \equiv t \cdot \gamma$') 
ax.set_ylabel('population fraction')
fig.savefig('out.pdf')
