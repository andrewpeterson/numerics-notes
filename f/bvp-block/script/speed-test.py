import numpy as np
import time
from scipy.optimize import fsolve
from matplotlib import pyplot
from numba import jit
import timeit


nx = 50
ny = 50
T_N = 400. # K
T_S = 200.
T_W = 320.
T_E = 290.
Lx = 1.
Ly = 1.
dx = Lx / nx
dy = Lx / ny

T = np.ones((nx, ny)) * (T_N + T_S + T_E + T_W) / 4.
def get_f(x):
    T = x.reshape((nx, ny))
    f = np.empty((nx, ny))
    # Interior points.
    for i in range(1, nx - 1):
        for j in range(1, ny - 1):
            f[i,j] = (T[i-1,j] - 2. * T[i,j] + T[i+1,j]) / dx**2
            f[i,j] += (T[i,j-1] - 2. * T[i,j] + T[i,j+1]) / dy**2
    # Edges.
    f[0,:] = T[0,:] - T_W
    f[-1,:] = T[-1,:] - T_E
    f[:,0] = T[:,0] - T_S
    f[:,-1] = T[:,-1] - T_N
    return f.reshape((nx * ny))

@jit(nopython=True)
def get_jit_f(x):
    T = x.reshape((nx, ny))
    f = np.empty((nx, ny))
    # Interior points.
    for i in range(1, nx - 1):
        for j in range(1, ny - 1):
            f[i,j] = (T[i-1,j] - 2. * T[i,j] + T[i+1,j]) / dx**2
            f[i,j] += (T[i,j-1] - 2. * T[i,j] + T[i,j+1]) / dy**2
    # Edges.
    f[0,:] = T[0,:] - T_W
    f[-1,:] = T[-1,:] - T_E
    f[:,0] = T[:,0] - T_S
    f[:,-1] = T[:,-1] - T_N
    return f.reshape((nx * ny))

get_jit_f(T)

ans = timeit.timeit('get_f(T)', number=1000, setup='from __main__ import get_f, T')
print(ans)


ans = timeit.timeit('get_jit_f(T)', number=1000, setup='from __main__ import get_jit_f, T')
print(ans)

