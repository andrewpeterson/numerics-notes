import numpy as np
from scipy.optimize import fsolve
from matplotlib import pyplot

def get_f(x):
    T = x.reshape((nx, ny))  #   <------------------------------Note!
    f = np.empty((nx, ny))
    # Interior points.
    for i in range(1, nx - 1):
        for j in range(1, ny - 1):
            f[i,j] = (T[i-1,j] - 2. * T[i,j] + T[i+1,j]) / dx**2
            f[i,j] += (T[i,j-1] - 2. * T[i,j] + T[i,j+1]) / dy**2
    # Edges.
    f[0,:] = T[0,:] - T_W
    f[-1,:] = T[-1,:] - T_E
    f[:,0] = T[:,0] - T_S
    f[:,-1] = T[:,-1] - T_N
    return f.reshape((nx * ny))  #  <---------------------------Note!

nx, ny = 50, 50
T_N, T_S, T_W, T_E = 400., 200., 320., 290. # K
Lx, Ly = 1., 1.
dx = Lx / (nx - 1.)
dy = Lx / (ny - 1.)
Tguess = np.ones((nx, ny)) * (T_N + T_S + T_E + T_W) / 4.
Tguess = Tguess.reshape(nx * ny)  #  <--------------------------Note!
ans = fsolve(func=get_f, x0=Tguess)
ans.resize((nx, ny))  #  <--------------------------------------Note!
fig, ax = pyplot.subplots()
ax.contourf(ans.T)
ax.set_xlabel('$x$ dimension')
ax.set_ylabel('$y$ dimension')
fig.savefig('twod.pdf')
