import numpy as np
import time
from scipy.optimize import fsolve
from matplotlib import pyplot
from numba import jit


nx = 40
ny = 40
T_N = 400. # K
T_S = 200.
T_W = 320.
T_E = 290.
Lx = 1.
Ly = 1.
dx = Lx / nx
dy = Lx / ny

T = np.ones((nx, ny)) * (T_N + T_S + T_E + T_W) / 4.

#@jit(nopython=True)
#@jit
def get_f(x):
    T = x.reshape((nx, ny))
    f = np.empty((nx, ny))
    # Interior points.
    for i in range(1, nx - 1):
        for j in range(1, ny - 1):
            f[i,j] = (T[i-1,j] - 2. * T[i,j] + T[i+1,j]) / dx**2
            f[i,j] += (T[i,j-1] - 2. * T[i,j] + T[i,j+1]) / dy**2
    # Edges.
    f[0,:] = T[0,:] - T_W
    f[-1,:] = T[-1,:] - T_E
    f[:,0] = T[:,0] - T_S
    f[:,-1] = T[:,-1] - T_N
    return f.reshape((nx * ny))

print('Compiling!')
get_f(T)
print('Optimizing.')

tic = time.time()
ans = fsolve(func=get_f, x0=T.reshape(nx * ny))
toc = time.time()
print('Duration:')
print(toc - tic)
ans.resize((nx, ny))
fig, ax = pyplot.subplots()
ax.contourf(ans.T)
ax.set_xlabel('$x$ dimension')
ax.set_xlabel('$y$ dimension')
fig.savefig('twod.pdf')
