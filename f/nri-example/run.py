import numpy as np

def get_f(x):
    return (np.exp(x) - 3. * x - 5.)

def get_fprime(x):
    return (np.exp(x) - 3.)

def get_new_x(x):
    return x - get_f(x) / get_fprime(x)

print('First run:')
x = 3.; print(x)
for _ in range(10):
    x = get_new_x(x); print(x)

print('Second run:')
x = -2.
for _ in range(10):
    x = get_new_x(x); print(x)
