import numpy as np
from matplotlib import pyplot
from matplotlib.patches import Circle

fig, ax = pyplot.subplots(figsize=(3.4, 3.))
fig.subplots_adjust(top=0.98, right=0.95, bottom=0.15, left=0.18)

ax.add_patch(Circle((-5., 0.), 2., edgecolor='C0', alpha=0.5))
ax.add_patch(Circle((-2., 0.), 2., edgecolor='C0', alpha=0.5))
ax.plot(-5., 0., '.', color='C0')
ax.plot(-2., 0., '.', color='C0')

ax.plot(-1., 0., 'x', color='C1')
ax.plot(-6., 0., 'x', color='C1')


ax.set_xlim(-10., 10.)
ax.set_ylim(-10., 10.)

ax.set_xlabel('Re($\lambda$)')
ax.set_ylabel('Im($\lambda$)')
ax.set_xticks([-10, -5., 0., 5., 10.])
ax.set_yticks([-10, -5., 0., 5., 10.])

fig.savefig('out.pdf')
