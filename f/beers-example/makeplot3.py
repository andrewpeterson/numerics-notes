import numpy as np
from matplotlib import pyplot


def get_f(x):
    return np.exp(x) - 3. * x - 5.


def get_f2(x):
    return (x - 2.53381039319)


def get_g(x):
    return get_f(x) / get_f2(x)


xs = np.linspace(-4., 4., num=1000)
fs = [get_f(x) for x in xs]
f2s = [get_f2(x) for x in xs]
gs = [get_g(x) for x in xs]

fig, axes = pyplot.subplots(nrows=3, figsize=(4., 8.))
fig.subplots_adjust(top=0.98, bottom=0.05, right=0.99, hspace=0.05, left=0.16)

ax = axes[0]
ax.plot(xs, fs, 'r-')
ax.set_xticklabels([])
ax.set_ylabel('$f$')
ax.text(0.2, 0.8, '$f(x) = e^x - 3x -5$', transform=ax.transAxes)

ax = axes[1]
ax.plot(xs, f2s, 'r-')
ax.set_xticklabels([])
ax.set_ylabel('$x-r_1$')
ax.text(0.2, 0.7, '$x-r_1$', transform=ax.transAxes)

ax = axes[2]
ax.plot(xs, gs, 'r-')
ax.set_ylim(-4., 16.)
ax.set_ylabel('$g$')
ax.set_xlabel('$x$')
ax.set_yticks([-4., 0., 4., 8., 12., 16.])
ax.text(0.2, 0.3, "$g(x) = \\frac{f(x)}{x-r_1}$", transform=ax.transAxes)

for ax in axes:
    ax.grid(True)
    ax.set_xticks([-4., -2., 0., 2., 4.])
    ax.plot([xs[0], xs[-1]], [0., 0.], '-k', lw=0.8)
    ax.set_xlim(xs[0], xs[-1])

fig.savefig('out3.pdf')
