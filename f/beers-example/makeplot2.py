import numpy as np
from matplotlib import pyplot


def get_f(x):
   return x**3 - 6. * x**2 + 11. * x - 6.


def get_f2(x):
    return (x - 1.)

def get_g(x):
    return get_f(x) / get_f2(x)


xs = np.linspace(0., 4., num=1000)
fs = [get_f(x) for x in xs]
f2s = [get_f2(x) for x in xs]
gs = [get_g(x) for x in xs]

fig, axes = pyplot.subplots(nrows=3, figsize=(4., 8.))
fig.subplots_adjust(top=0.98, bottom=0.05, right=0.99, hspace=0.05, left=0.16)

ax = axes[0]
ax.plot(xs, fs, 'r-')
ax.set_xticklabels([])
ax.set_ylabel('$f$')
ax.text(0.2, 0.8, '$f(x) = x^3 - 6 x^2 + 11 x - 6$', transform=ax.transAxes)

ax = axes[1]
ax.plot(xs, f2s, 'r-')
ax.set_xticklabels([])
ax.set_ylabel('$x-1$')
ax.text(0.2, 0.7, '$x-1$', transform=ax.transAxes)

ax = axes[2]
ax.plot(xs, gs, 'r-')
ax.set_ylim(-3., 3.)
ax.set_ylabel('$g_1$')
ax.set_xlabel('$x$')
ax.text(0.2, 0.3, "$g(x) = \\frac{f(x)}{x-1}$", transform=ax.transAxes)

for ax in axes:
    ax.grid(True)
    ax.set_xticks([0., 1., 2., 3., 4.])
    ax.plot([xs[0], xs[-1]], [0., 0.], '-k', lw=0.8)
    ax.set_xlim(xs[0], xs[-1])

fig.savefig('out2.pdf')
