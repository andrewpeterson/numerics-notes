#!/usr/bin/env python
"""
Starter script for matplotlib figures.
"""

import numpy as np
from matplotlib import pyplot


def makefig(figsize=(5., 5.), nrows=1, ncols=1,
            lm=0.1, rm=0.1, bm=0.1, tm=0.1, hg=0.1, vg=0.1,
            hr=None, vr=None):
    """
    figsize: canvas size
    nrows, ncols: number of horizontal and vertical images
    lm, rm, bm, tm, hg, vg: margins and "gaps"
    hr is horizontal ratio, and can be fed in as for example
    (1., 2.) to make the second axis twice as wide as the first.
    [same for vr]
    """
    nv, nh = nrows, ncols
    hr = np.array(hr, dtype=float) / np.sum(hr) if hr else np.ones(nh) / nh
    vr = np.array(vr, dtype=float) / np.sum(vr) if vr else np.ones(nv) / nv

    axwidths = (1. - lm - rm - (nh - 1.) * hg) * hr
    axheights = (1. - bm - tm - (nv - 1.) * vg) * np.array(vr)

    fig = pyplot.figure(figsize=figsize)
    axes = []
    bottompoint = 1. - tm
    for iv, v in enumerate(range(nv)):
        leftpoint = lm
        bottompoint -= axheights[iv]
        for ih, h in enumerate(range(nh)):
            ax = fig.add_axes((leftpoint, bottompoint,
                               axwidths[ih], axheights[iv]))
            axes.append(ax)
            leftpoint += hg + axwidths[ih]
        bottompoint -= vg
    return fig, axes


fig, axes = makefig(figsize=(4., 8.), nrows=3, vg=0, lm=0.2)

def get_f(x):
   return x**3 - 6. * x**2 + 11. * x - 6.

def get_fprime(x):
    return 3 * x**2 - 12. * x + 11.

xs = np.linspace(0., 4., num=1000)
fs = [get_f(x) for x in xs]
fprimes = [get_fprime(x) for x in xs]
updates = np.array(fs) / np.array(fprimes)

ax = axes[0]
ax.plot(xs, fs, 'r-')
ax.plot([xs[0], xs[-1]], [0., 0.], ':k')
ax.set_xticklabels([])
ax.set_ylabel('$f$')

ax = axes[1]
ax.plot(xs, fprimes, 'r-')
ax.plot([xs[0], xs[-1]], [0., 0.], ':k')
ax.set_xticklabels([])
ax.set_ylabel('$df/dx$')

ax = axes[2]
ax.plot(xs, updates, 'r-')
ax.plot([xs[0], xs[-1]], [0., 0.], ':k')
ax.set_ylim(-3., 3.)
ax.set_ylabel('update fxn')
ax.set_xlabel('$x$')





fig.savefig('out.pdf')
