#!/usr/bin/env python
"""
Starter script for matplotlib figures.
"""

import numpy as np
from matplotlib import pyplot
from matplotlib.patches import Circle


def makefig(figsize=(5., 5.), nrows=1, ncols=1,
            lm=0.1, rm=0.1, bm=0.1, tm=0.1, hg=0.1, vg=0.1,
            hr=None, vr=None):
    """
    figsize: canvas size
    nrows, ncols: number of horizontal and vertical images
    lm, rm, bm, tm, hg, vg: margins and "gaps"
    hr is horizontal ratio, and can be fed in as for example
    (1., 2.) to make the second axis twice as wide as the first.
    [same for vr]
    """
    nv, nh = nrows, ncols
    hr = np.array(hr, dtype=float) / np.sum(hr) if hr else np.ones(nh) / nh
    vr = np.array(vr, dtype=float) / np.sum(vr) if vr else np.ones(nv) / nv

    axwidths = (1. - lm - rm - (nh - 1.) * hg) * hr
    axheights = (1. - bm - tm - (nv - 1.) * vg) * np.array(vr)

    fig = pyplot.figure(figsize=figsize)
    axes = []
    bottompoint = 1. - tm
    for iv, v in enumerate(range(nv)):
        leftpoint = lm
        bottompoint -= axheights[iv]
        for ih, h in enumerate(range(nh)):
            ax = fig.add_axes((leftpoint, bottompoint,
                               axwidths[ih], axheights[iv]))
            axes.append(ax)
            leftpoint += hg + axwidths[ih]
        bottompoint -= vg
    return fig, axes


fig, axes = makefig(figsize=(5., 5.), bm=0.15, tm=0.05, lm=0.15, rm=0.05)
ax = axes[0]

centers = [0., -2.]
radii = [3., 1.]

for center, radius in zip(centers, radii):
    ax.plot(np.real(center), np.imag(center), 'r.')
    circle = Circle((np.real(center), np.imag(center)), radius)
    ax.add_patch(circle)

# Add true roots.
roots = np.roots([1., 2., 3.])
ax.plot(np.real(roots[0]), np.imag(roots[0]), 'xk')
ax.plot(np.real(roots[1]), np.imag(roots[1]), 'xk')

ax.set_xlabel('Re($\lambda$)')
ax.set_ylabel('Im($\lambda$)')
fig.savefig('out.pdf')
