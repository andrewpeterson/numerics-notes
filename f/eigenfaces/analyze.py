import numpy as np
from matplotlib import pyplot
from pca import get_images

def plot_recreated(n, ax, coefficients):
    """Plot a figure using n coefficients, putting the result on ax."""
    index = 0
    recreate = coefficients[index] * v[:,index]
    for _ in range(n - 1):
        index += 1
        recreate += coefficients[index] * v[:,index]
    ax.imshow(recreate.reshape(64, 64), cmap='inferno')
    ax.set_title(n)
    ax.set_xticks([])
    ax.set_yticks([])

def recreate_image(image_no):
    """Using various numbers of PC's, recreates the specified image."""
    plots = [1, 2, 3, 4, 5, 6, 10, 20, 50, 100, 200, 500, 1000, 4096]
    image = images[image_no].reshape(1, 4096)
    coefficients = np.matmul(image, v).reshape(4096)
    fig, axes = pyplot.subplots(ncols=14, nrows=1, figsize=(6.5, 1.))
    fig.subplots_adjust(left=0.01, right=0.99, bottom=0.01, top=0.95)
    axes = axes.reshape(14)
    for plot, ax in zip(plots, axes):
        plot_recreated(plot, ax, coefficients)
    fig.savefig('recreate-{}.pdf'.format(str(image_no)))

def plot_cumsum(w):
    """Makes a cumulative sum plot (for the eigenvalues)."""
    cumsum = np.cumsum(w / w.sum())
    fig, axes = pyplot.subplots(ncols=2, figsize=(6.5, 2.5), sharey=True)
    fig.subplots_adjust(left=0.08, bottom=0.17, top=0.99, right=0.99)
    axes[0].plot(cumsum, '-')
    axes[1].plot(cumsum[:100], '-')
    for ax in axes:
        ax.grid(True)
        ax.set_xlabel('principle component')
    axes[0].set_ylabel('eigenvalue cumulative sum')
    fig.savefig('cumsum.pdf')

def plot_PCs():
    """Plots the first several principal components on a single figure."""
    fig, axes = pyplot.subplots(nrows=2, ncols=10, figsize=(6.5, 2.))
    fig.subplots_adjust(left=0.01, right=0.99, bottom=0.01)
    axes = axes.reshape(20)
    for index, (ax, pc) in enumerate(zip(axes, v[:,:100].T)):
        ax.imshow(pc.reshape(64, 64), cmap='inferno')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_title('#{}'.format(str(index)))
    fig.savefig('pcs.pdf')

pca = np.load('pca.npz')
images = get_images()
v = np.real(pca['v'])
w = np.real(pca['w'])
plot_cumsum(w)
plot_PCs()
recreate_image(0)
recreate_image(9)
