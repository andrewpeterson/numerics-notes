import numpy as np
from matplotlib import pyplot
from zipfile import ZipFile
from PIL import Image

def get_images():
    """Gets a subset of the images from the database."""
    zf = ZipFile('lfwcrop_grey.zip')
    images = [_ for _ in zf.filelist if _.filename.endswith('.pgm')]
    randomstate = np.random.RandomState(42)  # make it repeatable
    images = randomstate.choice(images, size=100, replace=False)
    images = [Image.open(zf.open(_)) for _ in images]
    images = [np.array(_.getdata(), dtype=float)/255. - 0.5 for _ in images]
    return images

def plot_grid(images, ax):
    """Plots the images in nice grid on ax."""
    assert len(images) == 100
    data = np.empty((10 * 64, 10 * 64))
    for index, image in enumerate(images):
        row = index // 10
        col = index % 10
        data[row*64:row*64+64, col*64:col*64+64] = image.reshape(64, 64)
    ax.imshow(data, cmap='inferno')
    ax.set_xticks([])
    ax.set_yticks([])

if __name__ == '__main__':
    images = get_images()
    fig, ax = pyplot.subplots(figsize=(4., 4.))
    fig.subplots_adjust(left=.01, bottom=.01, top=.99, right=.99)
    plot_grid(images, ax)
    fig.savefig('raw-faces.pdf')

    # Perform the PCA and save it.
    sigma = 0.
    for image in images:
        x = np.reshape(image, (image.size, 1))  # Convert to vector.
        sigma += np.matmul(x, x.T)
    sigma /= len(images) - 1
    w, v = np.linalg.eig(sigma)
    np.savez_compressed('pca.npz', w=w, v=v)
