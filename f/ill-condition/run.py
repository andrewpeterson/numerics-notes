#!/usr/bin/env python

import numpy as np
from matplotlib import pyplot


class LinearSystem:
    """Convenience routines.
    Note this only works for 2-dimensional systems."""
    def __init__(self, A, b):
        self.A = A
        self.b = b

    def get_x1(self, x0, dim):
        """Basically solves the equation of a line for
        each dimension (row) of the system."""
        a0, a1 = self.A[dim]
        c = self.b[dim]
        return (c - a0 * x0) / a1

    def get_residual(self, x):
        """Returns the norm of the residual r = b - Ax
        for the vector x."""
        x = np.array(x).reshape([2, 1])
        r = self.b - np.matmul(self.A, x)
        return np.linalg.norm(r)


A_good = np.array([[0.3, -1.], [0.3, 1.]])
A_ill = np.array([[0.3, -1.], [0.31, -1.]])
b_good = np.array([[4., -2.]]).T
b_ill = np.array([[4., 4.]]).T
good = LinearSystem(A_good, b_good)
ill = LinearSystem(A_ill, b_ill)

fig, axes = pyplot.subplots(figsize=(6., 3.), ncols=2)
fig.subplots_adjust(bottom=0.15, left=0.08, right=0.99)
xlim = -10., 10.
ylim = -8., 2.

x0s = np.linspace(xlim[0], xlim[1])
x1s = np.linspace(ylim[0], ylim[1])

ax = axes[0]
ax.plot(x0s, good.get_x1(x0s, dim=0), '-')
ax.plot(x0s, good.get_x1(x0s, dim=1), '-')
ax.text(0.3, 0.9,
        '$0.3 x_1 - x_2 = 4$',
        transform=ax.transAxes, color='C0')
ax.text(0.3, 0.8,
        '$0.3 x_1 + x_2 = -2$',
        transform=ax.transAxes, color='C1')

ax = axes[1]
ax.plot(x0s, ill.get_x1(x0s, dim=0), '-')
ax.plot(x0s, ill.get_x1(x0s, dim=1), '-')
ax.text(0.1, 0.9,
        '$0.30 x_1 - x_2 = 4$',
        transform=ax.transAxes, color='C0')
ax.text(0.1, 0.8,
        '$0.31 x_1 - x_2 = 4$',
        transform=ax.transAxes, color='C1')

for ax in axes:
    ax.set_xlabel('$x_1$')
    ax.set_ylabel('$x_2$')
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)

fig.savefig('lines.pdf')

fig, axes = pyplot.subplots(figsize=(6., 2.5), ncols=2)
fig.subplots_adjust(bottom=0.17, left=0.08, right=0.94, wspace=0.4,
                    top=0.92)

x0m, x1m = np.meshgrid(x0s, x1s)
goodresids = np.empty_like(x0m)
illresids = np.empty_like(x0m)
for i in range(x0m.shape[0]):
    for j in range(x0m.shape[1]):
        x0 = x0m[i, j]
        x1 = x1m[i, j]
        goodresids[i, j] = good.get_residual([x0, x1])
        illresids[i, j] = ill.get_residual([x0, x1])

ax = axes[1]
illcontour = ax.contourf(x0m, x1m, illresids)
cbar = fig.colorbar(illcontour)
cbar.set_label('residual')

ax = axes[0]
cs = ax.contourf(x0m, x1m, goodresids, levels=illcontour.levels)
cbar = fig.colorbar(cs)
cbar.set_label('residual')


for ax in axes:
    ax.set_xlabel('$x_1$')
    ax.set_ylabel('$x_2$')
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
fig.savefig('residuals.pdf')
