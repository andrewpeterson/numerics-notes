import numpy as np
from matplotlib import pyplot


class NetPresentValue:
    """Helper class to calculate the net present value given annual cash flow
    information. `IRR` is the internal rate of return, expressed as a fraction
    (e.g., 0.08 is 8%). `annual_expenses` and `annual_incomes` are lists of
    numbers correspoding to the income and expense of each year for the
    analysis."""

    def __init__(self, IRR, annual_expenses, annual_incomes):
        self._IRR = IRR
        self._annual_expenses = annual_expenses
        self._annual_incomes = annual_incomes

    def get_present_value(self, year):
        cashflow = self._annual_incomes[year] - self._annual_expenses[year]
        presentvalue = cashflow / (1. + self._IRR)**year
        return presentvalue

    def get_net_present_value(self):
        """Returns the net present value over the course of the project.
        Also returns the annual present values."""
        print()
        print('{:>4s} {:>12s} {:>12s} {:>12s}'
              .format('Year', 'Cashflow', 'PV', 'NPV'))
        net_present_value = 0.
        presentvalues = []
        for year in range(len(self._annual_expenses)):
            cashflow = self._annual_incomes[year] - self._annual_expenses[year]
            presentvalue = self.get_present_value(year)
            net_present_value += presentvalue
            print('{:4d} {:+12.2f} {:+12.2f} {:+12.2f}'
                  .format(year, cashflow, presentvalue, net_present_value))
            presentvalues.append(presentvalue)
        return net_present_value, np.array(presentvalues)


years = 11
annual_expenses = np.zeros(years)
annual_expenses[0] = 9.
annual_incomes = 2. * np.ones(years)
npv_calc = NetPresentValue(0.10, annual_expenses, annual_incomes)
npv, pvs = npv_calc.get_net_present_value()

fig, ax = pyplot.subplots()
fig.subplots_adjust(top=0.99, right=0.99, bottom=0.02)
npvs = np.cumsum(pvs)
ax.bar(range(len(pvs)), pvs, label='present value')
ax.plot(range(len(npvs)), npvs, color='C1', label='net present value')
ax.set_xlabel('year')
ax.set_ylabel('(net) present value, $')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.set_xticks(range(years))
ax.legend()
fig.savefig('out.pdf')
