import numpy as np

A = np.array([[3., 1., 2.],
              [4., 6., 9.],
              [8., 5., 7.]])

# Create empty L matrix; we'll add nan's where
# we don't yet know the numbers (below the diagonal).
# The lower matrix holds the multipliers.
L = np.eye(3)
L = np.where(np.tril(np.ones_like(L), k=-1) == 1, np.nan, L)

# The upper matrix is the result of Gauss elim.
U = A.copy()

n = len(A)
for i in range(n):
    baserow = U[i]
    for j in range(i + 1, n):
        print(i, j)
        multiplier = - U[j, i] / U[i, i]
        print(multiplier)
        L[j, i] = - multiplier
        workingrow = U[j]
        newrow = workingrow + multiplier * baserow
        U[j] = newrow
        print(L)
        print(U)

print(L)
print(U)

B = np.matmul(L, U)
print(B)
