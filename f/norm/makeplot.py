from matplotlib import pyplot


fig, axes = pyplot.subplots(figsize=(8., 2.75), ncols=3)
fig.subplots_adjust(left=0.08, right=0.99, wspace=0.3, bottom=0.15)

point = (2., 3.)

# l1 norm
ax = axes[0]
ax.plot(point[0], point[1], 'o', color='C0')
ax.plot([0., point[0], point[0]], [0., 0., point[1]], '-', color='C0')
ax.set_title(r'$\ell_1$ norm')

# l2 norm
ax = axes[1]
ax.plot(point[0], point[1], 'o', color='C0')
ax.plot([0., point[0]], [0., point[1]], '-', color='C0')
ax.set_title(r'$\ell_2$ norm')

# linf norm
ax = axes[2]
ax.plot(point[0], point[1], 'o', color='C0')
ax.plot([0., 0.], [0., point[1]], '-', color='C0')
ax.set_title(r'$\ell_\infty$ norm')

for ax in axes:
    ax.set_xlabel('$x_1$')
    ax.set_ylabel('$x_2$')
    ax.axis('equal')
    ax.set_xlim([-.5, 3.5])
    ax.set_ylim([-.5, 3.5])
    ax.grid()

fig.savefig('out.pdf')
